/*
 * Copyright 2017 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.test;

import name.mlopatkin.shuttle.model.OffsetTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.SimpleTimeZone;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public final class TimeUtils {

    private TimeUtils() {
    }

    public static final TimeZone PDT = new SimpleTimeZone((int) (-TimeUnit.HOURS.toMillis(7)),
            "Test Los Angeles");
    public static final TimeZone UTC = new SimpleTimeZone(0, "Test UTC");
    public static final TimeZone MSK = new SimpleTimeZone((int) TimeUnit.HOURS.toMillis(3),
            "Test Moscow");
    public static final TimeZone NST = new SimpleTimeZone(
            (int) (TimeUnit.HOURS.toMillis(5) + TimeUnit.MINUTES.toMillis(45)), "Test Kathmandu");

    public static Date parseDateTime(String time) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mmXXX").parse(time);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static long parseTimestamp(String time) {
        return parseDateTime(time).getTime();
    }

    public static OffsetTime offsetFromString(String time) {
        return offsetFromString(time, UTC);
    }

    public static OffsetTime offsetFromString(String time, TimeZone tz) {
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(new SimpleDateFormat("HH:mm").parse(time));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        return new OffsetTime(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),
                tz);
    }
}
