/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.test;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.is;

import com.google.common.base.Function;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import name.mlopatkin.shuttle.model.Location;
import name.mlopatkin.shuttle.model.Route;

public class ModelMatchers {

    public static Matcher<Route> hasOrigin(Location from) {
        return getterMatcher(Route::getFrom, is(from), "Route from");
    }

    public static Matcher<Route> hasDestination(Location to) {
        return getterMatcher(Route::getTo, is(to), "Route to");
    }

    public static Matcher<Route> isBetween(Location from, Location to) {
        return both(hasOrigin(from)).and(hasDestination(to));
    }

    public static <T, U> Matcher<T> getterMatcher(Function<T, U> getter, Matcher<U> valueMatcher,
            String getterDescription) {
        return new TypeSafeMatcher<T>() {
            @Override
            protected boolean matchesSafely(T item) {
                return valueMatcher.matches(getter.apply(item));
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(getterDescription).appendDescriptionOf(valueMatcher);
            }
        };
    }
}
