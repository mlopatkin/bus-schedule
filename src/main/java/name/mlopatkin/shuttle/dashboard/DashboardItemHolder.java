/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.dashboard;

import android.view.View;
import android.widget.TextView;

import name.mlopatkin.shuttle.R;
import name.mlopatkin.utils.android.Views;

/**
 * The holder for a departure info view. It is very dumb for now and displays
 * times as given.
 */
public final class DashboardItemHolder {

    private final TextView mDepartureTime;
    private final TextView mDepartureInterval;

    private DashboardItemHolder(View itemView) {
        mDepartureTime = Views.findViewById(itemView, R.id.departure_time);
        mDepartureInterval = Views.findViewById(itemView, R.id.departure_interval);
    }

    /**
     * Updates view to display given departureTime (e.g. 18:00).
     *
     * @param departureTime the time to display
     * @param tagTime locale-independent departure time format for ui tests (hh:mm)
     */
    public void setDepartureTime(CharSequence departureTime, CharSequence tagTime) {
        mDepartureTime.setText(departureTime);
        mDepartureTime.setTag(tagTime);
    }

    /**
     * Updates view to display given departureInterval (e.g. in 5 minutes).
     *
     * @param departureInterval the interval to display
     */
    public void setDepartureInterval(CharSequence departureInterval) {
        mDepartureInterval.setText(departureInterval);
    }

    /**
     * Gets a holder from the given View. Can return <code>null</code> if there
     * is no holder attached.
     *
     * @param itemView the view to get a holder from
     * @return the holder or <code>null</code> if there is no one
     */
    public static DashboardItemHolder from(View itemView) {
        return (DashboardItemHolder) itemView.getTag();
    }

    /**
     * Creates a holder for the given view and attaches it.
     *
     * @param itemView the header view
     * @return the holder that is already attached to the view
     */
    public static DashboardItemHolder createFor(View itemView) {
        return Views.attachHolder(itemView, new DashboardItemHolder(itemView));
    }
}
