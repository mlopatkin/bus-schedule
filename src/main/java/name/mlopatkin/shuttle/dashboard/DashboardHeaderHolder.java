/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.dashboard;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import name.mlopatkin.shuttle.R;
import name.mlopatkin.utils.android.Views;

/**
 * ViewHolder for dashboard header.
 */
public final class DashboardHeaderHolder {
    private final TextView mLocationTitle;
    private final Drawable mArrowDrawable;

    private DashboardHeaderHolder(View headerView) {
        mLocationTitle = Views.findViewById(headerView, R.id.location_title);
        mArrowDrawable = mLocationTitle.getResources().getDrawable(R.drawable.forward_arrow);
    }

    /**
     * Gets a holder from the given View. Can return <code>null</code> if there
     * is no holder attached.
     *
     * @param headerView the view to get a holder from
     * @return the holder or <code>null</code> if there is no one
     */
    public static DashboardHeaderHolder from(View headerView) {
        return (DashboardHeaderHolder) headerView.getTag();
    }

    /**
     * Creates a holder for the given view and attaches it.
     *
     * @param headerView the header view
     * @return the holder that is already attached to the view
     */
    public static DashboardHeaderHolder createFor(View headerView) {
        return Views.attachHolder(headerView, new DashboardHeaderHolder(headerView));
    }

    /**
     * Updates the header view with the given location title.
     *
     * @param title the title of the location
     */
    public void setLocationTitle(CharSequence title) {
        mLocationTitle.setText(title);
    }

    public void setArrowForInbound() {
        mLocationTitle.setCompoundDrawablesWithIntrinsicBounds(null, null, mArrowDrawable, null);
    }

    public void setArrowForOutbound() {
        mLocationTitle.setCompoundDrawablesWithIntrinsicBounds(mArrowDrawable, null, null, null);
    }
}
