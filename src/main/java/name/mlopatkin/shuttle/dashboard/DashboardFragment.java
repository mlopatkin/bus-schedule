/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.dashboard;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import androidx.annotation.Nullable;

import com.google.common.base.Optional;
import dagger.Lazy;
import dagger.android.AndroidInjection;

import name.mlopatkin.shuttle.HeartBeatSender;
import name.mlopatkin.shuttle.R;
import name.mlopatkin.shuttle.fullschedule.ScheduleActivity;
import name.mlopatkin.shuttle.locationlist.SelectedLocation;
import name.mlopatkin.shuttle.locationlist.SelectedLocationChangeObserver;
import name.mlopatkin.shuttle.model.Location;
import name.mlopatkin.shuttle.model.Route;
import name.mlopatkin.utils.android.Views;

import java.util.List;

import javax.inject.Inject;

/**
 * The fragment that can show several next route entries for a location.
 * Instantiator may select to display inbound or outbound locations.
 */
public class DashboardFragment extends Fragment implements AdapterView.OnItemClickListener,
        HeartBeatSender.Observer, SelectedLocationChangeObserver {

    private ListView mListView;
    private DashboardListAnimator mListAnimator;

    private Location mLocation;
    private int mDisplayMode = DISPLAY_OUTBOUND_ROUTES;

    public static final int DISPLAY_INBOUND_ROUTES = 0;
    public static final int DISPLAY_OUTBOUND_ROUTES = 1;

    @Inject
    HeartBeatSender mHeartBeatSender;

    @Inject
    SelectedLocation mSelectedLocation;

    @Inject
    Lazy<DashboardListAdapter> mRoutesListAdapter;

    @Override
    public void onAttach(Activity activity) {
        AndroidInjection.inject(this);
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mRoutesListAdapter.get().displayOutboundRoutes();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mSelectedLocation.addLocationChangeObserver(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View fragmentRoot = inflater.inflate(R.layout.dashboard_fragment, container, false);

        mListView = Views.findViewById(fragmentRoot, android.R.id.list);
        mListView.setEmptyView(fragmentRoot.findViewById(android.R.id.empty));
        mListView.setAdapter(mRoutesListAdapter.get());
        resetAdapterData(false);

        mListAnimator = new DashboardListAnimator(mListView);

        mListView.setOnItemClickListener(this);
        return fragmentRoot;
    }

    @Override
    public void onStart() {
        super.onStart();
        mHeartBeatSender.addObserver(this);
    }

    @Override
    public void onStop() {
        mHeartBeatSender.removeObserver(this);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        mSelectedLocation.removeLocationChangeObserver(this);
        super.onDestroy();
    }

    @Override
    public void onLocationChanged(Location newLocation, boolean isNear) {
        mLocation = newLocation;
        setDisplayedRoutes(isNear ? DISPLAY_OUTBOUND_ROUTES : DISPLAY_INBOUND_ROUTES);
        resetAdapterData(true);
    }

    @Override
    public void onTimeTick(boolean isNewDay) {
        List<Integer> expiredPositions = mRoutesListAdapter.get().getExpiredPositions();
        if (!expiredPositions.isEmpty()) {
            // something is going away, we should animate it
            mListAnimator.startItemRemoval(expiredPositions, () -> resetAdapterData(false));
        } else {
            // just update remaining time labels
            if (!isNewDay) {
                mRoutesListAdapter.get().notifyDataSetChanged();
            } else {
                resetAdapterData(true);
            }
        }
    }

    private void resetAdapterData(boolean resetAnimations) {
        if (resetAnimations) {
            mListAnimator.reset();
        }
        if (mLocation != null) {
            mRoutesListAdapter.get().setData(mLocation);
        }
    }

    public void setDisplayedRoutes(int displayType) {
        displayType &= DISPLAY_OUTBOUND_ROUTES;
        if (mDisplayMode == displayType) {
            return;
        }
        mDisplayMode = displayType;
        switch (mDisplayMode) {
            case DISPLAY_INBOUND_ROUTES:
                mRoutesListAdapter.get().displayInboundRoutes();
                break;
            case DISPLAY_OUTBOUND_ROUTES:
                mRoutesListAdapter.get().displayOutboundRoutes();
                break;
        }
        resetAdapterData(true);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Optional<Route> route = mRoutesListAdapter.get().getRoute(position);
        if (route.isPresent()) {
            ScheduleActivity.startForRoute(getActivity(), route.get());
        }
    }
}
