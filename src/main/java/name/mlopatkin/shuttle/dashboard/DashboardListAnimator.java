/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.dashboard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.ListView;

import java.util.HashMap;
import java.util.List;

/**
 * Helper class that performs animations when dashboard content changes.
 * <p/>
 * The animation is triggered by items being removed (and probably replaced by others). It runs in
 * three phases. At first, items being removed fade out. Then remaining items shift to new places.
 * After that new items fade in.
 */
class DashboardListAnimator {

    private static final int ENTRY_FADE_OUT_DURATION_MS = 150;
    private static final int ENTRY_FADE_IN_DURATION_MS = 150;
    private static final int ENTRY_MOVE_TO_PLACE_DURATION_MS = 150;

    private final ListView mListView;
    private final HashMap<Long, Integer> mTopById = new HashMap<>();

    private Runnable mRemover;
    private List<Integer> mPositionsToRemove;

    private final Animator.AnimatorListener mRemoveAnimationCompleted =
            new AnimatorListenerAdapter() {
                @Override
                public void onAnimationCancel(Animator animation) {
                    // nothing good is happening, just remove and forget about it
                    mRemover.run();
                    mListView.post(mResetRunnable);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    proceedRemoving();
                }
            };

    private final Animator.AnimatorListener mInAnimationCompleted =
            new AnimatorListenerAdapter() {

                @Override
                public void onAnimationCancel(Animator animation) {
                    mListView.post(mResetRunnable);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mListView.post(mResetRunnable);
                }
            };

    private final ViewTreeObserver.OnPreDrawListener mListLayoutCompleted =
            new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    mListView.getViewTreeObserver().removeOnPreDrawListener(this);
                    int childCount = mListView.getChildCount();
                    int firstVisiblePos = mListView.getFirstVisiblePosition();

                    ViewPropertyAnimator animatorWithListener = null;
                    boolean firstAppearanceAnimation = true;
                    for (int i = 0; i < childCount; i++) {
                        View view = mListView.getChildAt(i);
                        int pos = firstVisiblePos + i;
                        long id = mListView.getItemIdAtPosition(pos);
                        if (mTopById.get(id) != null) {
                            int oldTop = mTopById.get(id);
                            int newTop = view.getTop();
                            view.setTranslationY(oldTop - newTop);
                            ViewPropertyAnimator animator = view.animate();
                            if (animatorWithListener == null) {
                                // somebody must take care of the listener
                                animatorWithListener = animator;
                                animator.setListener(mInAnimationCompleted);
                            } else {
                                animator.setListener(null);
                            }
                            animator.setDuration(ENTRY_MOVE_TO_PLACE_DURATION_MS).translationY(0)
                                    .start();
                        } else {
                            view.setAlpha(0);
                            ViewPropertyAnimator animator = view.animate();
                            if (firstAppearanceAnimation) {
                                // now we have appearance animation
                                firstAppearanceAnimation = false;
                                if (animatorWithListener != null) {
                                    // If we had set up translation previously - we may clear it.
                                    // This appearance must take care of listener because it
                                    // completes later
                                    animatorWithListener.setListener(null);
                                }
                                animatorWithListener = animator;
                                animator.setListener(mInAnimationCompleted);
                            } else {
                                animator.setListener(null);
                            }
                            animator.setStartDelay(ENTRY_MOVE_TO_PLACE_DURATION_MS)
                                    .setDuration(ENTRY_FADE_IN_DURATION_MS)
                                    .alpha(1)
                                    .start();
                        }
                    }
                    return true;
                }
            };

    private final Runnable mResetRunnable = this::reset;

    public DashboardListAnimator(ListView listView) {
        mListView = listView;
    }

    public void startItemRemoval(List<Integer> positionsToRemove,
            Runnable removeContinuation) {
        mRemover = removeContinuation;
        mPositionsToRemove = positionsToRemove;

        mListView.setEnabled(false);

        int firstVisiblePos = mListView.getFirstVisiblePosition();
        int lastVisiblePos = mListView.getLastVisiblePosition();

        boolean firstAnimation = true;
        for (int pos : mPositionsToRemove) {
            if (pos >= firstVisiblePos && pos <= lastVisiblePos) {
                int childIndex = pos - firstVisiblePos;
                ViewPropertyAnimator animator = mListView.getChildAt(childIndex).animate();
                if (firstAnimation) {
                    firstAnimation = false;
                    animator.setListener(mRemoveAnimationCompleted);
                } else {
                    animator.setListener(null);
                }
                animator.alpha(0).setDuration(ENTRY_FADE_OUT_DURATION_MS).start();
            }
        }
    }

    private void collectViewPositions() {
        // save positions of existing views to animate them from there to new positions
        mTopById.clear();
        int childCount = mListView.getChildCount();
        int firstVisiblePos = mListView.getFirstVisiblePosition();
        for (int i = 0; i < childCount; i++) {
            if (!mPositionsToRemove.contains(i + firstVisiblePos)) {
                View child = mListView.getChildAt(i);
                int pos = firstVisiblePos + i;
                mTopById.put(mListView.getItemIdAtPosition(pos), child.getTop());
            }
        }
    }

    private void proceedRemoving() {
        collectViewPositions();
        mRemover.run();
        mListView.getViewTreeObserver().addOnPreDrawListener(mListLayoutCompleted);
    }

    public void reset() {
        mRemover = null;
        mPositionsToRemove = null;
        mTopById.clear();
        mListView.setEnabled(true);
        int childCount = mListView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = mListView.getChildAt(i);
            view.animate().setListener(null);
            view.setAlpha(1);
            view.setTranslationY(0);
        }
    }
}
