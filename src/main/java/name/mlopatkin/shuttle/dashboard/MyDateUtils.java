/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.dashboard;

import android.content.Context;
import android.content.res.Resources;
import android.text.format.DateUtils;

import name.mlopatkin.shuttle.R;
import name.mlopatkin.utils.android.Assertions;

/**
 * Utility class with workarounds for bugs in {@link DateUtils} class.
 */
public final class MyDateUtils {

    private MyDateUtils() {
        Assertions.staticOnly();
    }

    /**
     * Formats a relative time interval between {@code now} and {@code time}
     * with a minimal minute precision. E.g. if there are less than 60000 ms
     * between {@code now} and {@code time} then the result will be
     * "in 0 minutes" in appropriate language.
     *
     * @param context the context
     * @param time the timestamp of some moment in the future
     * @param now the current timestamp
     * @return the formatted string "in X minutes/hours/days"
     */
    public static CharSequence getRelativeTimeSpanString(Context context, long time, long now) {
        Resources resources = context.getResources();
        long delta = time - now;
        if (delta < 0) {
            return resources.getText(R.string.already);
        }

        int resId;
        long count;

        if (delta < DateUtils.HOUR_IN_MILLIS) {
            count = delta / DateUtils.MINUTE_IN_MILLIS;
            resId = R.plurals.in_num_minutes;
        } else if (delta < DateUtils.DAY_IN_MILLIS) {
            count = delta / DateUtils.HOUR_IN_MILLIS;
            resId = R.plurals.in_num_hours;
        } else if (delta < DateUtils.DAY_IN_MILLIS * 2) {
            return resources.getString(R.string.tomorrow);
        } else {
            count = delta / DateUtils.DAY_IN_MILLIS;
            resId = R.plurals.in_num_days;
        }

        return resources.getQuantityString(resId, (int) count, (int) count);
    }
}
