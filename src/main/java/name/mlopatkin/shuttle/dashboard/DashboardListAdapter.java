/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.dashboard;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;

import static name.mlopatkin.utils.android.Assertions.notReached;

import name.mlopatkin.shuttle.R;
import name.mlopatkin.shuttle.TimeSource;
import name.mlopatkin.shuttle.model.Location;
import name.mlopatkin.shuttle.model.Route;
import name.mlopatkin.shuttle.model.RouteEntry;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

/**
 * List adapter to display dashboard: soonest buses from selected locations.
 */
public class DashboardListAdapter extends BaseAdapter {

    private final Context mContext;
    private final LayoutInflater mInflater;
    private final TimeSource mTimeSource;

    private static final int ITEM_TYPE_HEADER = 0;
    private static final int ITEM_TYPE_DEPARTURE = 1;
    private static final int ITEM_TYPE_LAST = ITEM_TYPE_DEPARTURE;

    private static final int MAX_ENTRIES_PER_LOCATION = 3;

    private ListMultimap<Route, RouteEntry> mData = ImmutableListMultimap.of();
    private int[] mRouteHeaderPositions = new int[0];
    private List<Route> mRoutes = Collections.emptyList();

    private Location mSelectedLocation;
    private boolean mDisplayInbound = false;

    /**
     * Creates a new list adapter. It holds reference to the context for the
     * whole lifetime.
     *
     * @param context the context
     */
    @Inject
    public DashboardListAdapter(Context context, TimeSource timeSource) {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mTimeSource = timeSource;
    }

    public DashboardListAdapter displayInboundRoutes() {
        mDisplayInbound = true;
        setData(mSelectedLocation);
        return this;
    }

    public DashboardListAdapter displayOutboundRoutes() {
        mDisplayInbound = false;
        setData(mSelectedLocation);
        return this;
    }

    private void updateHeaderIndices() {
        int locCount = mRoutes.size();
        mRouteHeaderPositions = new int[locCount];
        if (locCount == 0) {
            return;
        }
        mRouteHeaderPositions[0] = 0;
        int i = 0;
        for (Route route : mRoutes) {
            if (i + 1 < locCount) {
                mRouteHeaderPositions[i + 1] = mRouteHeaderPositions[i]
                        + mData.get(route).size() + 1;
            }
            ++i;
        }
    }

    /**
     * Updates data to be displayed in this adapter.
     *
     * @param location the new location
     */
    public void setData(Location location) {
        mSelectedLocation = location;
        if (mSelectedLocation != null) {
            if (mDisplayInbound) {
                mData = mSelectedLocation.getNearestArrivals(mTimeSource.getCurrentTimestamp(),
                        MAX_ENTRIES_PER_LOCATION);
            } else {
                mData = mSelectedLocation.getNearestDepartures(mTimeSource.getCurrentTimestamp(),
                        MAX_ENTRIES_PER_LOCATION);
            }
            mRoutes = ImmutableList.copyOf(mData.keySet());
        } else {
            mRoutes = ImmutableList.of();
        }
        updateHeaderIndices();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size() + mRoutes.size();
    }

    private int getHeaderIndex(int position) {
        int headerIndex = Arrays.binarySearch(mRouteHeaderPositions, position);
        if (headerIndex >= 0) {
            return headerIndex;
        } else {
            return -headerIndex - 2;
        }
    }

    private Route getHeader(int position) {
        return mRoutes.get(getHeaderIndex(position));
    }

    private RouteEntry getDeparture(int position) {
        int headerIndex = getHeaderIndex(position);
        int headerPos = mRouteHeaderPositions[headerIndex];
        int offset = position - headerPos - 1;
        return mData.get(mRoutes.get(headerIndex)).get(offset);
    }

    @Override
    public Object getItem(int position) {
        if (getItemViewType(position) == ITEM_TYPE_HEADER) {
            return getHeader(position);
        } else {
            return getDeparture(position);
        }
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public long getItemId(int position) {
        if (getItemViewType(position) == ITEM_TYPE_HEADER) {
            return getHeader(position).getId() << 32;
        } else {
            return getDeparture(position).getId();
        }
    }

    @Override
    public int getItemViewType(int position) {
        int headerPos = Arrays.binarySearch(mRouteHeaderPositions, position);
        if (headerPos >= 0) {
            return ITEM_TYPE_HEADER;
        } else {
            return ITEM_TYPE_DEPARTURE;
        }
    }

    @Override
    public int getViewTypeCount() {
        return ITEM_TYPE_LAST + 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = newView(position, parent);
        }
        bindView(position, convertView, parent);
        return convertView;
    }

    private void bindView(int position, View convertView, ViewGroup parent) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ITEM_TYPE_HEADER: {
                DashboardHeaderHolder header = DashboardHeaderHolder.from(convertView);
                Route route = getHeader(position);
                Location location = mDisplayInbound ? route.getFrom() : route.getTo();
                header.setLocationTitle(
                        mDisplayInbound ? location.getTitleFrom() : location.getTitleTo());
                break;
            }
            case ITEM_TYPE_DEPARTURE: {
                RouteEntry entry = getDeparture(position);
                long now = mTimeSource.getCurrentTimestamp();
                long depTime = entry.getTodaysDepartureTimeAtPresentLocation(now);
                CharSequence depTimeStr = DateUtils.formatDateTime(mContext, depTime,
                        DateUtils.FORMAT_SHOW_TIME);

                CharSequence tagTime = String.format(Locale.US, "%02d:%02d",
                        entry.getTime().getHours(), entry.getTime().getMinutes());
                convertView.setAlpha(1.0f);
                DashboardItemHolder.from(convertView).setDepartureTime(depTimeStr, tagTime);

                CharSequence depTimeInterval = MyDateUtils.getRelativeTimeSpanString(mContext,
                        depTime, now);
                DashboardItemHolder.from(convertView).setDepartureInterval(depTimeInterval);
                break;
            }
            default:
                throw notReached("Unexpected view type=" + viewType);
        }
    }

    private View newHeaderView(int position, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.dashboard_list_header, parent, false);
        DashboardHeaderHolder.createFor(view);
        return view;
    }

    private View newDepartureView(int position, ViewGroup parent) {
        View view = mInflater.inflate(R.layout.dashboard_list_item, parent, false);
        DashboardItemHolder.createFor(view);
        return view;
    }

    private View newView(int position, ViewGroup parent) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case ITEM_TYPE_HEADER:
                return newHeaderView(position, parent);
            case ITEM_TYPE_DEPARTURE:
                return newDepartureView(position, parent);
            default:
                throw notReached("Unexpected view type=" + viewType);
        }
    }

    public List<Integer> getExpiredPositions() {
        long now = mTimeSource.getCurrentTimestamp();
        ArrayList<Integer> r = Lists.newArrayListWithCapacity(2);
        int count = getCount();
        for (int i = 0; i < count; ++i) {
            if (getItemViewType(i) == ITEM_TYPE_DEPARTURE) {
                RouteEntry entry = getDeparture(i);
                if (entry.getTodaysDepartureTimeAtPresentLocation(now) < now) {
                    r.add(i);
                }
            }
        }
        return r;
    }

    public Optional<Route> getRoute(int position) {
        if (getItemViewType(position) == ITEM_TYPE_HEADER) {
            return Optional.of(getHeader(position));
        }
        return Optional.absent();
    }
}
