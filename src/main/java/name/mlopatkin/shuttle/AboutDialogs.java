/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import name.mlopatkin.utils.android.Assertions;
import name.mlopatkin.utils.android.Views;

public final class AboutDialogs {

    private AboutDialogs() {
        Assertions.staticOnly();
    }

    public static void showAboutDialog(Activity activity) {
        new AboutDialog().show(activity.getFragmentManager(), "about_dialog");
    }

    public static void show3rdPartyLicensesDialog(Activity activity) {
        new ThirdPartyLicensesFragment().show(activity.getFragmentManager(), "licenses_dialog");
    }

    public static class AboutDialog extends DialogFragment {

        private String getAppVersion() {
            String packageName = getActivity().getPackageName();
            try {
                PackageInfo info = getActivity().getPackageManager().getPackageInfo(packageName, 0);
                return info.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                return "N/A";
            }
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            View dialogBody = Views.inflateDialogBody(getActivity(), R.layout.about_dialog);

            TextView versionView = Views.findViewById(dialogBody, R.id.about_dialog_version);
            versionView.setText(getString(R.string.about_dialog_version, getAppVersion()));

            TextView messageView = Views.findViewById(dialogBody, R.id.about_dialog_message);
            messageView.setText(R.string.about_dialog_message);

            TextView thirdPartyLicense = Views.findViewById(dialogBody, R.id.about_dialog_3rdparty);
            thirdPartyLicense.setOnClickListener(v -> show3rdPartyLicensesDialog(getActivity()));
            SpannableString thirdPartyCaption =
                    new SpannableString(getString(R.string.about_dialog_3rd_party));
            thirdPartyCaption.setSpan(new UnderlineSpan(), 0 , thirdPartyCaption.length(), 0);
            thirdPartyLicense.setText(thirdPartyCaption);
            return new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.menu_about_app)
                    .setView(dialogBody)
                    .setCancelable(true)
                    .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                    .create();
        }
    }

    public static class ThirdPartyLicensesFragment extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            WebView dialogBody = new WebView(getActivity());

            dialogBody.loadUrl("file:///android_asset/3rdparty-licenses.html");

            return new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.about_dialog_3rd_party)
                    .setView(dialogBody)
                    .setCancelable(true)
                    .setPositiveButton("OK", (dialog, which) -> dialog.dismiss())
                    .create();
        }
    }
}
