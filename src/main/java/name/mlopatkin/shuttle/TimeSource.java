/*
 * Copyright 2017 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import java.util.Calendar;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * The class to provide current time. It only exists for testing.
 */
@Singleton
public class TimeSource {
    @Inject
    TimeSource() {}

    public Calendar getCurrentTime() {
        return Calendar.getInstance();
    }

    public long getCurrentTimestamp() {
        return System.currentTimeMillis();
    }
}
