/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.fullschedule;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.collect.Lists;
import dagger.android.AndroidInjection;

import name.mlopatkin.shuttle.R;
import name.mlopatkin.shuttle.db.ShuttleDatabase;
import name.mlopatkin.shuttle.model.Location;
import name.mlopatkin.shuttle.model.Route;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class RouteListFragment extends DialogFragment implements AdapterView.OnItemClickListener {

    private static final int LIST_ITEM_LAYOUT = android.R.layout.simple_list_item_1;

    @Inject
    ShuttleDatabase mDatabase;

    public static void showRouteSelector(Activity owner) {
        new RouteListFragment().show(owner.getFragmentManager(), "route_select_dialog");
    }

    private ArrayAdapter<Route> mAdapter;

    private List<Route> getAllRoutes() {
        List<Location> locations = mDatabase.getLocations();
        ArrayList<Route> routes = Lists.newArrayListWithCapacity(4);
        for (Location loc : locations) {
            routes.addAll(loc.getRoutesFrom());
        }
        return routes;
    }

    @Override
    public void onAttach(Activity activity) {
        AndroidInjection.inject(this);
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mAdapter = new ArrayAdapter<Route>(getActivity(), LIST_ITEM_LAYOUT, getAllRoutes()) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                Route route = getItem(position);
                if (convertView == null) {
                    convertView = inflater.inflate(LIST_ITEM_LAYOUT, parent, false);
                }
                TextView text = (TextView) convertView;
                text.setText(Formatters.formatRoute(getActivity(), route));
                return convertView;
            }
        };
        ListView listView = new ListView(getActivity());
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(this);
        return listView;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.setTitle(R.string.show_full_timetable_dialog_title);
        return dialog;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ScheduleActivity.startForRoute(getActivity(), mAdapter.getItem(position));
        dismiss();
    }
}
