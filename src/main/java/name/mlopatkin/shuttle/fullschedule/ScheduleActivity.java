/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.fullschedule;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.common.base.Optional;
import dagger.android.AndroidInjection;

import name.mlopatkin.shuttle.BaseDbActivity;
import name.mlopatkin.shuttle.R;
import name.mlopatkin.shuttle.model.Route;
import name.mlopatkin.shuttle.model.Tag;

public class ScheduleActivity extends BaseDbActivity implements ScheduleFragment.DataProvider {

    private static final String EXTRA_ROUTE_TAG = "name.mlopatkin.shuttle.extra.ROUTE_TAG";

    private Route mRoute;

    public static void startForRoute(Context context, Route route) {
        context.startActivity(
                new Intent(context, ScheduleActivity.class)
                        .putExtra(EXTRA_ROUTE_TAG, route.getTag())
        );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDatabaseReady(Bundle savedInstanceState) {
        super.onDatabaseReady(savedInstanceState);

        Tag<Route> routeTag = getIntent().getParcelableExtra(EXTRA_ROUTE_TAG);
        Optional<Route> route = getDatabase().getRouteByTag(routeTag);
        if (!route.isPresent()) {
            // Database changed and current route isn't available anymore. Go back.
            finish();
            return;
        }

        mRoute = route.get();

        ActionBar actionBar = getActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(Formatters.formatRoute(this, mRoute));
        setContentView(R.layout.activity_schedule);
    }

    @Override
    public Route getRoute() {
        return mRoute;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            if (item.getItemId() == android.R.id.home) {
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
