/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.fullschedule;

import android.app.Activity;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ListView;

import dagger.android.AndroidInjection;

import name.mlopatkin.shuttle.HeartBeatSender;
import name.mlopatkin.shuttle.R;
import name.mlopatkin.shuttle.TimeSource;
import name.mlopatkin.shuttle.model.Route;

import javax.inject.Inject;

public class ScheduleFragment extends ListFragment {

    private ScheduleAdapter mAdapter;

    interface DataProvider {
        Route getRoute();
    }

    private DataProvider mDataProvider;

    @Inject
    HeartBeatSender mHeartBeatSender;

    @Inject
    TimeSource mTimeSource;

    @Override
    public void onAttach(Activity activity) {
        AndroidInjection.inject(this);
        super.onAttach(activity);
        mDataProvider = (DataProvider) activity;
    }

    @Override
    public void onDetach() {
        mDataProvider = null;
        super.onDetach();
    }

    @Override
    public void onStart() {
        super.onStart();
        mHeartBeatSender.addObserver(mAdapter);
    }

    @Override
    public void onStop() {
        mHeartBeatSender.removeObserver(mAdapter);
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        final int highlightPosition = mAdapter.getHighlightPosition();
        if (highlightPosition >= 0) {
            final ListView listView = getListView();
            if (listView.getChildCount() == 0 && listView.getCount() > 0) {
                // list view isn't ready yet
                listView.getViewTreeObserver()
                        .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                            @SuppressWarnings("deprecation")
                            @Override
                            public void onGlobalLayout() {
                                listView.smoothScrollToPosition(highlightPosition);
                                // need to support API 14
                                listView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            }
                        });
            } else {
                listView.smoothScrollToPosition(highlightPosition);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.schedule_fragment, container, false);
        mAdapter = new ScheduleAdapter(getActivity(), mTimeSource, mDataProvider.getRoute());
        setListAdapter(mAdapter);
        return root;
    }
}
