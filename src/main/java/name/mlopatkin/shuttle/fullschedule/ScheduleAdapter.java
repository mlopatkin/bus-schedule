/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.fullschedule;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import name.mlopatkin.shuttle.HeartBeatSender;
import name.mlopatkin.shuttle.R;
import name.mlopatkin.shuttle.TimeSource;
import name.mlopatkin.shuttle.model.Route;
import name.mlopatkin.shuttle.model.RouteEntry;
import name.mlopatkin.utils.android.HighlightListView;

import java.util.List;

public class ScheduleAdapter extends BaseAdapter implements
        HighlightListView.ListAdapterWithHighlight, HeartBeatSender.Observer {

    private final Context mContext;
    private final TimeSource mTimeSource;
    private final List<RouteEntry> mEntries;

    private int mCachedHighlightPosition;

    public ScheduleAdapter(Context context, TimeSource timeSource, Route route) {
        mContext = context;
        mTimeSource = timeSource;
        mEntries = route.getEntries();
        mCachedHighlightPosition = calculateHighlightPosition();
    }

    @Override
    public int getCount() {
        return mEntries.size();
    }

    @Override
    public RouteEntry getItem(int position) {
        return mEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = newView(position, parent);
        }
        return bindView(position, convertView);
    }

    private View bindView(int position, View view) {
        long now = mTimeSource.getCurrentTimestamp();
        long timestamp = getItem(position).getTodaysDepartureTimeAtPresentLocation(now);

        TextView textView = (TextView) view;
        if (timestamp > now) {
            textView.setTextColor(mContext.getResources().getColor(R.color.default_departure_text));
        } else {
            textView.setTextColor(mContext.getResources().getColor(R.color.past_departure_text));
        }
        CharSequence depTimeStr = DateUtils.formatDateTime(mContext, timestamp,
                DateUtils.FORMAT_SHOW_TIME);
        textView.setText(depTimeStr);
        return view;
    }

    private View newView(int position, ViewGroup parent) {
        return LayoutInflater.from(mContext).inflate(R.layout.schedule_list_item, parent, false);
    }

    private int calculateHighlightPosition() {
        long now = mTimeSource.getCurrentTimestamp();
        for (int i = 0; i < mEntries.size(); ++i) {
            RouteEntry e = mEntries.get(i);
            if (now < e.getTodaysDepartureTimeAtPresentLocation(now)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onTimeTick(boolean isNewDay) {
        mCachedHighlightPosition = calculateHighlightPosition();
        notifyDataSetChanged();
    }

    @Override
    public int getHighlightPosition() {
        return mCachedHighlightPosition;
    }
}
