/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import android.app.Activity;
import android.app.Application;
import android.app.Fragment;
import android.content.Context;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasFragmentInjector;

import name.mlopatkin.shuttle.db.ShuttleDatabase;

import javax.inject.Inject;

/**
 * Main application object, a holder for all global facilities.
 */
public class ShuttlesApp extends Application implements HasActivityInjector, HasFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Activity> mDispatchingActivityInjector;

    @Inject
    DispatchingAndroidInjector<Fragment> mDispatchingFragmentInjector;

    @Inject
    ShuttleDatabase mShuttleDatabase;

    @Override
    public void onCreate() {
        super.onCreate();

        makeApplicationComponent().inject(this);

        mShuttleDatabase.startAsyncLoading();
    }

    /**
     * Helper method to avoid tedious casting when retrieving a system service.
     * It can be used in situations like:
     *
     * <pre>
     * FooBarService foo = getService(context, FOOBAR_SERVICE);
     * </pre>
     *
     * This method also takes care about extracting application context from
     * supplied context.
     *
     * @param context the valid context
     * @param name the name of the service
     * @return the system service casted to appropriate type
     * @param <T> the type of service.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getSystemService(Context context, String name) {
        return (T) context.getApplicationContext().getSystemService(name);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return mDispatchingActivityInjector;
    }

    @Override
    public AndroidInjector<Fragment> fragmentInjector() {
        return mDispatchingFragmentInjector;
    }

    protected ApplicationComponent makeApplicationComponent() {
        return DaggerApplicationComponent.builder().applicationModule(
                new ApplicationModule(this)).build();
    }
}
