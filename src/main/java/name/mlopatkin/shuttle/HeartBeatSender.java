/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;

import name.mlopatkin.utils.ObserverList;

import java.util.Calendar;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * The service, just not in Android way, that sends heartbeats every minute to
 * all interested subscribers.
 */
@Singleton
public class HeartBeatSender {

    @VisibleForTesting
    public static final String ACTION_HEARTBEAT_TEST = "name.mlopatkin.shuttle.HEARTBEAT_TEST";

    private static final String TAG = "Heartbeat";

    public interface Observer {
        void onTimeTick(boolean isNewDay);
    }

    private final Context mContext;
    private final TimeSource mTimeSource;
    private final ObserverList<Observer> mObservers = new ObserverList<>();

    private Calendar mLastBroadcast;

    private BroadcastReceiver mTimeReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            broadcastEvent();
        }
    };

    @Inject
    HeartBeatSender(Context context, TimeSource timeSource) {
        mContext = context.getApplicationContext();
        mTimeSource = timeSource;
    }

    private void broadcastEvent() {
        boolean isNewDay = false;
        Calendar newBroadcast = mTimeSource.getCurrentTime();
        if (mLastBroadcast != null) {
            // we should invalidate our assumptions in three cases
            // 1. day change
            // 2. timezone change
            // 3. time is adjusted backwards
            isNewDay =
                    !Objects.equal(mLastBroadcast.getTimeZone(), newBroadcast.getTimeZone()) ||
                            mLastBroadcast.get(Calendar.DAY_OF_MONTH) !=
                                    newBroadcast.get(Calendar.DAY_OF_MONTH)
                            || mLastBroadcast.after(newBroadcast);
        }
        mLastBroadcast = newBroadcast;
        for (Observer obs : mObservers) {
            obs.onTimeTick(isNewDay);
        }
    }

    /**
     * Resumes paused heartbeats. Resume also triggers a broadcast right away.
     */
    private void resume() {
        Log.d(TAG, "Enable heartbeat" );
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_TIME_TICK);
        filter.addAction(Intent.ACTION_TIME_CHANGED);
        filter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        if (BuildConfig.DEBUG) {
            filter.addAction(ACTION_HEARTBEAT_TEST);
        }
        mContext.registerReceiver(mTimeReceiver, filter);
    }

    /**
     * Pauses heartbeats. No heartbeats will be sent until {@link #resume()}.
     */
    private void pause() {
        Log.d(TAG, "Disable heartbeat");
        mContext.unregisterReceiver(mTimeReceiver);
    }

    public void addObserver(Observer observer) {
        if (mObservers.isEmpty()) {
            resume();
        }
        mObservers.addObserver(observer);
        broadcastEvent();
    }

    public void removeObserver(Observer observer) {
        mObservers.removeObserver(observer);
        if (mObservers.isEmpty()) {
            pause();
        }
    }
}
