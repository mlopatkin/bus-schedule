/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import android.app.Activity;
import android.os.Bundle;

import dagger.android.AndroidInjection;

import name.mlopatkin.shuttle.db.ShuttleDatabase;

import javax.inject.Inject;

/**
 * Base class for all activities in this project. It provides common services like displaying a
 * spinner while loading a database.
 */
public abstract class BaseDbActivity extends Activity implements ShuttleDatabase.Observer {

    private Bundle mSavedState;

    private boolean mResumed;

    @Inject
    ShuttleDatabase mShuttleDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        if (mShuttleDatabase.isLoaded()) {
            onDatabaseReady(savedInstanceState);
        } else {
            mSavedState = savedInstanceState;
            waitForDatabase();
        }
    }

    private void waitForDatabase() {
        setContentView(R.layout.activity_loader);
        mShuttleDatabase.addObserver(this);
    }

    @Override
    public void onScheduleLoaded() {
        onDatabaseReady(mSavedState);
        mShuttleDatabase.removeObserver(this);
    }

    protected void onDatabaseReady(Bundle savedInstanceState) {
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mResumed = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mResumed = false;
    }

    @Override
    protected void onDestroy() {
        mShuttleDatabase.removeObserver(this);
        super.onDestroy();
    }

    public boolean isResumeCalled() {
        return mResumed;
    }

    public ShuttleDatabase getDatabase() {
        return mShuttleDatabase;
    }
}
