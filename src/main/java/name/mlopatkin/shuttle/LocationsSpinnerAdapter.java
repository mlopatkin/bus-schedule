/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import name.mlopatkin.shuttle.db.ShuttleDatabase;
import name.mlopatkin.shuttle.location.LocationAutodetector;
import name.mlopatkin.shuttle.location.PositionProvider;
import name.mlopatkin.shuttle.locationlist.SelectedLocation;
import name.mlopatkin.shuttle.locationlist.SelectedLocationChangeObserver;
import name.mlopatkin.shuttle.model.Location;
import name.mlopatkin.utils.ObserverList;
import name.mlopatkin.utils.android.ActivityLifecycleCallbacksAdapter;
import name.mlopatkin.utils.android.Contexts;

/**
 * The adapter for the list of all locations. The ID notion is slightly different for this adapter
 * and doesn't always match a Location.getId() because of autodetect pseudolocation.
 */
@ActivityScope
public class LocationsSpinnerAdapter extends BaseAdapter implements SelectedLocation {

    private interface LocationEntry {

        Location getLocation();

        void setSelected(boolean selected);

        long getId();

        boolean isNear();
    }

    private final Context mContext;
    private final LayoutInflater mInflater;
    private final ArrayList<LocationEntry> mLocations = Lists.newArrayListWithCapacity(4);
    private final Preferences mPreferences;
    private final ObserverList<SelectedLocationChangeObserver> mLocationChangeObservers =
            new ObserverList<>();

    private LocationEntry mSelectedEntry;

    @Inject
    public LocationsSpinnerAdapter(MainActivity activity, ShuttleDatabase db,
            Preferences preferences, PositionProvider positionProvider) {
        boolean startResumed = activity.isResumeCalled();
        List<Location> locations = db.getLocations();

        mContext = activity;
        mInflater = LayoutInflater.from(mContext);
        mPreferences = preferences;

        LocationAutodetector detector =
                LocationAutodetector.create(positionProvider, locations, locations.get(0));
        AutoDetectEntry autoDetectEntry = new AutoDetectEntry(startResumed, detector);
        detector.setDetectedLocationListener(autoDetectEntry);
        mLocations.add(autoDetectEntry);

        Contexts.addScopedActivityCallbacks(activity, autoDetectEntry);

        for (Location loc : locations) {
            mLocations.add(new ValueEntry(loc));
        }

        long selectedId = mPreferences.getSelectedDashboardLocationId();
        for (int i = 0; i < mLocations.size(); ++i) {
            if (getItemId(i) == selectedId) {
                setSelected(i);
                break;
            }
        }
        if (mSelectedEntry == null) {
            setSelected(0);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return createAndBindView(position, convertView, parent,
                R.layout.dashboard_location_selector_item, false);
    }

    private View createAndBindView(int position, View convertView, ViewGroup parent, int layoutId,
            boolean isDropDown) {
        View view = convertView;
        if (view == null) {
            view = mInflater.inflate(layoutId, parent, false);
        }
        TextView textView = (TextView) view;
        Location item = getItem(position);
        if (mLocations.get(position) instanceof AutoDetectEntry) {
            AutoDetectEntry autoDetect = (AutoDetectEntry) (mLocations.get(position));
            int pinResource = R.drawable.ic_action_location_searching;
            if (autoDetect.isReliable() && !isDropDown) {
                pinResource = R.drawable.ic_action_location_found;
            }
            Drawable pin = mContext.getResources().getDrawable(pinResource);
            if (isDropDown) {
                textView.setCompoundDrawablesWithIntrinsicBounds(null, null, pin, null);
                textView.setText(R.string.pseudolocation_autodetect);
            } else {
                textView.setCompoundDrawablesWithIntrinsicBounds(pin, null, null, null);
                if (autoDetect.isReliable()) {
                    textView.setText(item.getTitleFrom());
                } else {
                    textView.setText(item.getTitleTo());
                }
            }
        } else {
            textView.setCompoundDrawables(null, null, null, null);
            textView.setText(item.getTitleFrom());
        }
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return createAndBindView(position, convertView, parent,
                R.layout.dashboard_location_selector_drop_item, true);
    }

    @Override
    public long getItemId(int position) {
        return mLocations.get(position).getId();
    }

    @Override
    public int getCount() {
        return mLocations.size();
    }

    @Override
    public Location getItem(int position) {
        return mLocations.get(position).getLocation();
    }

    public Location getSelectedLocation() {
        return mSelectedEntry.getLocation();
    }

    public int getSelectedPosition() {
        return mLocations.indexOf(mSelectedEntry);
    }

    @Override
    public void addLocationChangeObserver(SelectedLocationChangeObserver observer) {
        mLocationChangeObservers.addObserver(observer);
        if (mSelectedEntry != null) {
            observer.onLocationChanged(mSelectedEntry.getLocation(), mSelectedEntry.isNear());
        }
    }

    @Override
    public void removeLocationChangeObserver(SelectedLocationChangeObserver observer) {
        mLocationChangeObservers.removeObserver(observer);
    }

    private void notifyLocationChange(Location location, boolean isNear) {
        for (SelectedLocationChangeObserver observer : mLocationChangeObservers) {
            observer.onLocationChanged(location, isNear);
        }
    }

    public void setSelected(int position) {
        if (mSelectedEntry == mLocations.get(position)) {
            return;
        }
        if (mSelectedEntry != null) {
            mSelectedEntry.setSelected(false);
        }
        mSelectedEntry = mLocations.get(position);
        mSelectedEntry.setSelected(true);
        mPreferences.setSelectedDashboardLocationId(mSelectedEntry.getId());
        notifyDataSetChanged();
    }

    private class AutoDetectEntry extends ActivityLifecycleCallbacksAdapter
            implements LocationEntry, LocationAutodetector.DetectedLocationListener {

        private static final long AUTODETECT_LOCATION_ID = -2;

        private final LocationAutodetector mDetector;

        private boolean mSelected;
        private boolean mActivityResumed;

        AutoDetectEntry(boolean activityResumed, LocationAutodetector detector) {
            mDetector = detector;
            mActivityResumed = activityResumed;
        }

        public boolean isReliable() {
            return mDetector.isDetectedLocationNear();
        }

        @Override
        public Location getLocation() {
            return mDetector.getDetectedLocation();
        }

        @Override
        public void setSelected(boolean selected) {
            updateState(selected, mActivityResumed);
            if (selected) {
                notifyLocationChange(mDetector.getDetectedLocation(),
                        mDetector.isDetectedLocationNear());
            }
        }

        @Override
        public long getId() {
            return AUTODETECT_LOCATION_ID;
        }

        @Override
        public boolean isNear() {
            return mDetector.isDetectedLocationNear();
        }

        @Override
        public void onActivityResumed(Activity activity) {
            updateState(mSelected, true);
        }

        @Override
        public void onActivityPaused(Activity activity) {
            updateState(mSelected, false);
        }

        private void updateState(boolean selected, boolean resumed) {
            mSelected = selected;
            mActivityResumed = resumed;
            if (mSelected && mActivityResumed) {
                mDetector.start();
            } else {
                mDetector.stop();
            }
        }

        @Override
        public void onDetectedLocationUpdate(Location newLocation, boolean isNear) {
            if (mSelected) {
                notifyLocationChange(newLocation, isNear);
            }
            notifyDataSetChanged();
        }
    }

    private class ValueEntry implements LocationEntry {

        private final Location mLocation;

        ValueEntry(Location location) {
            mLocation = location;
        }

        @Override
        public Location getLocation() {
            return mLocation;
        }

        @Override
        public boolean isNear() {
            return true;
        }

        @Override
        public void setSelected(boolean selected) {
            if (selected) {
                notifyLocationChange(mLocation, true);
            }
        }

        @Override
        public long getId() {
            return mLocation.getId();
        }
    }
}
