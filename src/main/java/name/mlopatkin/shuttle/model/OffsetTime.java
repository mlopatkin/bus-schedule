/*
 * Copyright 2017 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.model;

import com.google.common.base.Preconditions;
import com.google.common.primitives.Ints;

import java.util.Calendar;
import java.util.Formatter;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * This class represents a time: basically what is written on a clock, e. g. 12:30. There
 * is no notion of date.
 * <p/>
 * It has a limited precision (only hours and minutes are supported) because it is enough for
 * purposes of this application.
 * <p/>
 * This class is immutable.
 * <p/>
 * See also java.time.OffsetTime. This class is poor man's backport of it.
 */
public class OffsetTime {
    private final int mMidnightOffsetMs;
    private final TimeZone mTimeZone;

    public OffsetTime(int hours, int minutes, TimeZone timeZone) {
        Preconditions.checkArgument(0 <= hours && hours < 24, "Invalid hours value %d", hours);
        Preconditions.checkArgument(0 <= minutes && minutes < 60, "Invalid minutes value %d",
                minutes);
        // int is fine because MAX meaningful offset is 23:59:59.999 < 86400000
        mMidnightOffsetMs = Ints.checkedCast(
                TimeUnit.HOURS.toMillis(hours) + TimeUnit.MINUTES.toMillis(minutes));
        mTimeZone = Preconditions.checkNotNull(timeZone);
    }

    /**
     * Returns a timestamp (a concrete moment in time, as ms from the start of epoch) when wall
     * clock shows the time encoded in this instance today. The date of "Today" is derived from a
     * value of the parameter and the timezone offset. The returned timestamp may be in the past, if
     * the encoded time already happened today.
     *
     * @param now current time as ms from the start of epoch
     */
    public long getTodayInstant(long now) {
        Calendar cal = Calendar.getInstance(mTimeZone);
        cal.setTimeInMillis(now);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        cal.add(Calendar.MILLISECOND, mMidnightOffsetMs);
        return cal.getTimeInMillis();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OffsetTime that = (OffsetTime) o;

        return mMidnightOffsetMs == that.mMidnightOffsetMs && mTimeZone.equals(that.mTimeZone);
    }

    @Override
    public int hashCode() {
        int result = mMidnightOffsetMs;
        result = 31 * result + mTimeZone.hashCode();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("OffsetTime{");
        formatHHMM(mMidnightOffsetMs, false, result);
        formatHHMM(mTimeZone.getOffset(System.currentTimeMillis()), true, result);
        result.append(" / ").append(mMidnightOffsetMs).append(" ms}");
        return result.toString();
    }

    private static void formatHHMM(long offsetMs, boolean includeSign,
            StringBuilder builder) {
        long offsetVal = Math.abs(offsetMs);
        long hours = TimeUnit.MILLISECONDS.toHours(offsetVal);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(offsetVal - TimeUnit.HOURS.toMillis(hours));
        if (offsetMs < 0) {
            builder.append('-');
        } else if (includeSign) {
            builder.append('+');
        }
        try (Formatter timeUnitFormatter = new Formatter(builder, Locale.US)) {
            timeUnitFormatter.format("%02d:%02d", hours, minutes);
        }
    }

    public TimeZone getTimeZone() {
        return mTimeZone;
    }

    public long getHours() {
        return TimeUnit.MILLISECONDS.toHours(mMidnightOffsetMs);
    }

    public long getMinutes() {
        return TimeUnit.MILLISECONDS.toMinutes(mMidnightOffsetMs) - TimeUnit.HOURS.toMinutes(
                getHours());
    }
}
