/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.model;

import com.google.common.base.Function;
import com.google.common.base.MoreObjects;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The single entry for the route in the timetable. For example, {@link Route}
 * from Benua to Novocherkasskaya can have several entries: at 11:10, at 11:30
 * and so on.
 * <p/>
 * The instances of this class are immutable.
 */
public class RouteEntry extends DbEntry {

    static final Function<RouteEntry, Route> GET_ROUTE = RouteEntry::getRoute;

    private final Route mRoute;

    private final OffsetTime mTime;

    /**
     * Creates a route entry for the given route.
     *
     * @param route the route
     * @param time the local time of the departure
     */
    private RouteEntry(long id, Route route, OffsetTime time) {
        super(id);
        mRoute = route;
        mTime = time;
    }

    public Route getRoute() {
        return mRoute;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((mRoute == null) ? 0 : mRoute.hashCode());
        result = prime * result + mTime.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        RouteEntry other = (RouteEntry) obj;
        if (mRoute == null) {
            if (other.mRoute != null) {
                return false;
            }
        } else if (!mRoute.equals(other.mRoute)) {
            return false;
        }
        if (mTime.equals(other.mTime)) {
            return false;
        }
        return true;
    }

    /**
     * Returns a timestamp of departure assuming that it happened/happens at the
     * day that is {@code now} at the departure location.
     *
     * @param now the current timestamp
     * @return the timestamp of today's departure
     */
    public long getTodaysDepartureTimeAtPresentLocation(long now) {
        return mTime.getTodayInstant(now);
    }

    private String getStringTime() {
        DateFormat dateFormat = SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT);
        dateFormat.setTimeZone(mTime.getTimeZone());
        return dateFormat.format(
                new Date(getTodaysDepartureTimeAtPresentLocation(System.currentTimeMillis())));
    }

    public OffsetTime getTime() {
        return mTime;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", getId())
                .add("from", mRoute.getFrom().getName())
                .add("to", mRoute.getTo().getName())
                .add("time", getStringTime())
                .toString();
    }

    public static class Builder {
        private final long mId;
        private final OffsetTime mTime;

        Builder(long id, OffsetTime time) {
            mId = id;
            mTime = time;
        }

        RouteEntry build(Route route) {
            return new RouteEntry(mId, route, mTime);
        }
    }
}
