/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.model;


import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * The route. The route connects two {@link Location}s.
 * <p>
 * The instances of this class are immutable.
 */
public class Route extends DbEntry {
    private final Location mFrom;
    private final Location mTo;

    private ImmutableList<RouteEntry> mEntries;

    private Route(long id, Location from, Location.Builder to, List<RouteEntry.Builder> entries,
            Builder builder) {
        super(id);
        mFrom = from;
        builder.mIncompleteRoute = this;
        mTo = to.build();
        ImmutableList.Builder<RouteEntry> entriesBuilder = ImmutableList.builder();
        for (RouteEntry.Builder entry : entries) {
            entriesBuilder.add(entry.build(this));
        }
        mEntries = entriesBuilder.build();
    }

    public Location getFrom() {
        return mFrom;
    }

    public Location getTo() {
        return mTo;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((mFrom == null) ? 0 : mFrom.hashCode());
        result = prime * result + ((mTo == null) ? 0 : mTo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (((Object) this).getClass() != obj.getClass()) {
            return false;
        }
        Route other = (Route) obj;
        if (mFrom == null) {
            if (other.mFrom != null) {
                return false;
            }
        } else if (!mFrom.equals(other.mFrom)) {
            return false;
        }
        if (mTo == null) {
            if (other.mTo != null) {
                return false;
            }
        } else if (!mTo.equals(other.mTo)) {
            return false;
        }
        return true;
    }

    public List<RouteEntry> getEntries() {
        return mEntries;
    }

    public ImmutableList<RouteEntry> getNextEntries(long now) {
        int i = 0;
        while (i < mEntries.size() && mEntries.get(i)
                                            .getTodaysDepartureTimeAtPresentLocation(now) < now) {
            ++i;
        }
        return mEntries.subList(i, mEntries.size());
    }

    public Tag<Route> getTag() {
        return Tag.makeTag(this);
    }

    public static class Builder {
        private final long mId;
        private final TimeZone mTimeZone;
        private final Location.Builder mTo;

        private final List<RouteEntry.Builder> mEntries = new ArrayList<>();
        private Route mIncompleteRoute;

        Builder(long id, TimeZone timeZone, Location.Builder to) {
            mId = id;
            mTimeZone = timeZone;
            mTo = to;
        }

        public Builder addRouteEntry(long id, int hours, int minutes) {
            mEntries.add(new RouteEntry.Builder(id, new OffsetTime(hours, minutes, mTimeZone)));
            return this;
        }

        Route build(Location from) {
            if (mIncompleteRoute != null) {
                return mIncompleteRoute;
            }
            return new Route(mId, from, mTo, mEntries, this);
        }
    }

    @Override
    public String toString() {
        return "Route{" +
                "mId=" + getId() +
                ", mFrom=" + mFrom +
                ", mTo=" + mTo +
                '}';
    }
}
