/*
 * Copyright 2017 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.model;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * This class is a complete schedule - the data model of the application.
 * <p/>
 * The instances of this class are immutable. Use {@link Builder} to construct it.
 */
public class Schedule {

    private final ImmutableList<Location> mLocations;

    private Schedule(List<Location.Builder> locations) {
        ImmutableList.Builder<Location> builder = ImmutableList.builder();
        for (Location.Builder location : locations) {
            builder.add(location.build());
        }
        mLocations = builder.build();
    }

    /**
     * @return a list of all known locations
     */
    public ImmutableList<Location> getLocations() {
        return mLocations;
    }

    /**
     * Finds a route by its "tag".
     *
     * @param tag the tag
     * @return a route with this tag
     */
    public Optional<Route> getRouteByTag(Tag<Route> tag) {
        for (Location loc : mLocations) {
            for (Route route : loc.getRoutesFrom()) {
                if (tag.isTagFor(route)) {
                    return Optional.of(route);
                }
            }
        }
        return Optional.absent();
    }

    /**
     * Builds a schedule step-by-step. This class is not safe to use after a call to {@link
     * #build()}.
     */
    public static class Builder {
        private final List<Location.Builder> mLocations = new ArrayList<>();

        /**
         * Adds a new location to the schedule. Returns a {@link Location.Builder} that can be used
         * to add routes from this location. The returned builder is valid until a call to {@link
         * #build()} of THIS builder.
         *
         * @param id the id of the location
         * @param name the displayable name of the location, used in "Foo - Bar" messages
         * @param titleFrom the displayable name used in "From Foo" messages
         * @param titleTo the displayable name used in "To Foo" messages
         * @param timeZone the native timezone of the location
         * @param lat the latitude of the location
         * @param lon the longitude of the location
         * @return the builder to add routes
         */
        public Location.Builder addLocation(long id, String name, String titleFrom,
                String titleTo, TimeZone timeZone, long lat, long lon) {
            Location.Builder builder = new Location.Builder(id, name, titleFrom, titleTo, timeZone,
                    lat, lon);
            mLocations.add(builder);
            return builder;
        }

        /**
         * @return a new instance of Schedule.
         */
        public Schedule build() {
            return new Schedule(mLocations);
        }
    }
}

