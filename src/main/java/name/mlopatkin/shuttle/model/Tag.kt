/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * A parcelable holder for a model value. Model values (locations, routes, etc) aren't guaranteed to
 * be always present across updates, even ids can change.
 * @param <T>
 */
abstract class Tag<T> : Parcelable {

    /**
     * Checks if the supplied value matches this tag.
     * @param value the value to check
     * @return `true` if this tag matches the value.
     */
    abstract fun isTagFor(value: T): Boolean

    @Parcelize
    internal data class RouteTag(val from: String, val to: String) : Tag<Route>() {
        override fun isTagFor(value: Route): Boolean {
            return this == makeTag(value)
        }
    }

    companion object {
        /**
         * Make a tag for a route.
         * @param route the route to make a tag for
         * @return the tag
         */
        @JvmStatic
        fun makeTag(route: Route): Tag<Route> {
            return RouteTag(route.from.name, route.to.name)
        }
    }
}
