/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.model;

import androidx.annotation.VisibleForTesting;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimaps;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

/**
 * The location. It represents an endpoint of the shuttle bus route.
 * <p/>
 * The instances of this class are immutable.
 */
public class Location extends DbEntry {

    private final String mName;

    private final String mTitleFrom;

    private final String mTitleTo;

    private final long mLat;

    private final long mLon;

    private ImmutableList<Route> mRoutes;
    private ImmutableList<Route> mRoutesTo;

    /**
     * Creates a location with the given name. All routes from this location
     * should have departure times in the specified time zone.
     *
     * @param name the name of the location
     * @param titleFrom the variation of the name of the location in form "From NAME"
     * @param titleTo the variation of the name of the location in form "To NAME"
     */
    @VisibleForTesting
    public Location(String name, String titleFrom, String titleTo, long lat, long lon) {
        super(NO_ID);
        mName = Preconditions.checkNotNull(name);
        mTitleFrom = titleFrom;
        mTitleTo = titleTo;
        mLat = lat;
        mLon = lon;

        mRoutes = ImmutableList.of();
        mRoutesTo = ImmutableList.of();
    }

    private Location(Builder builder, long id, String name, String titleFrom, String titleTo,
            long lat, long lon, List<Route.Builder> routesFrom, List<Route.Builder> routesTo) {
        super(id);
        mName = Preconditions.checkNotNull(name);
        mTitleFrom = titleFrom;
        mTitleTo = titleTo;
        mLat = lat;
        mLon = lon;

        // Escaping this to allow other route builders to re-use our reference. The building is
        // thread-confined so it should be safe to observe incomplete objects.
        builder.mIncompleteLocation = this;

        mRoutes = buildRoutes(routesFrom);
        mRoutesTo = buildRoutes(routesTo);
    }

    private ImmutableList<Route> buildRoutes(List<Route.Builder> routes) {
        ImmutableList.Builder<Route> routesBuilder = ImmutableList.builder();
        for (Route.Builder route : routes) {
            routesBuilder = routesBuilder.add(route.build(this));
        }
        return routesBuilder.build();
    }

    public String getName() {
        return mName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((mName == null) ? 0 : mName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Location other = (Location) obj;
        if (mName == null) {
            if (other.mName != null) {
                return false;
            }
        } else if (!mName.equals(other.mName)) {
            return false;
        }
        return true;
    }

    public String getTitleFrom() {
        return mTitleFrom;
    }

    public String getTitleTo() {
        return mTitleTo;
    }

    public long getLat() {
        return mLat;
    }

    public long getLon() {
        return mLon;
    }

    public List<Route> getRoutesFrom() {
        return mRoutes;
    }

    private <I> ListMultimap<I, RouteEntry> getGroupedNearestEntries(long now, int limit,
            List<Route> routes, Function<RouteEntry, I> groupBy) {
        List<RouteEntry> entries = Lists.newArrayListWithCapacity(limit * routes.size());
        for (Route route : routes) {
            List<RouteEntry> nextEntries = route.getNextEntries(now);
            entries.addAll(nextEntries.subList(0, Math.min(limit, nextEntries.size())));
        }
        return Multimaps.index(entries, groupBy);
    }

    public ListMultimap<Route, RouteEntry> getNearestDepartures(long now, int limit) {
        return getGroupedNearestEntries(now, limit, getRoutesFrom(), RouteEntry.GET_ROUTE);
    }

    public ListMultimap<Route, RouteEntry> getNearestArrivals(long now, int limit) {
        return getGroupedNearestEntries(now, limit, getRoutesTo(), RouteEntry.GET_ROUTE);
    }

    @Override
    public String toString() {
        return "Location{name=" + mName + "}";
    }

    public List<Route> getRoutesTo() {
        return mRoutesTo;
    }

    /**
     * The builder for a location. Allows to add {@link Route}s from it.
     */
    public static class Builder {
        private final long mId;
        private final String mName;
        private final String mTitleFrom;
        private final String mTitleTo;
        private final TimeZone mTimeZone;
        private final long mLat;
        private final long mLon;

        private final List<Route.Builder> mRoutesFrom = new ArrayList<>();
        private final List<Route.Builder> mRoutesTo = new ArrayList<>();

        private Location mIncompleteLocation;

        Builder(long id, String name, String titleFrom, String titleTo, TimeZone timeZone, long lat,
                long lon) {
            mId = id;
            mName = name;
            mTitleFrom = titleFrom;
            mTitleTo = titleTo;
            mTimeZone = timeZone;
            mLat = lat;
            mLon = lon;
        }

        /**
         * Adds a route from this location.
         * @param id the unique id of the route
         * @param to the destination
         * @return the {@link Route.Builder} object to add {@link RouteEntry}s
         */
        public Route.Builder addRoute(long id, Location.Builder to) {
            Route.Builder route = new Route.Builder(id, mTimeZone, to);
            mRoutesFrom.add(route);
            to.mRoutesTo.add(route);
            return route;
        }


        Location build() {
           if (mIncompleteLocation != null) {
               return mIncompleteLocation;
           }
            return new Location(this, mId, mName, mTitleFrom, mTitleTo,
                    mLat, mLon, mRoutesFrom, mRoutesTo);
        }
    }
}
