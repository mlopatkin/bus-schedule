/*
 * Copyright 2018 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.net;

import android.content.Context;
import android.util.Log;
import androidx.annotation.AnyThread;
import androidx.annotation.WorkerThread;

import com.google.common.io.ByteSource;
import com.google.common.io.Files;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * A downloadable web schedule.
 */
@Singleton
@AnyThread
public class WebScheduleSource {
    private static final String TAG = "WebScheduleSource";

    private static final String WEB_SCHEDULE_URL =
            "https://shuttle.mlopatkin.name/v1/schedule.json";

    private final File mDownloadedScheduleLocation;

    @Inject
    WebScheduleSource(Context context) {
        mDownloadedScheduleLocation = new File(context.getFilesDir(), "schedule.json");
    }

    /**
     * Checks if the cached web schedule is available. This method hits the disk.
     *
     * @return {@code true} if there is a cached schedule
     */
    @WorkerThread
    public boolean isAvailable() {
        return mDownloadedScheduleLocation.exists();
    }

    /**
     * Removes a cached schedule.
     */
    @WorkerThread
    public void removeSavedSchedule() {
        if (mDownloadedScheduleLocation.exists() && !mDownloadedScheduleLocation.delete()) {
            Log.e(TAG, "Failed to remove saved schedule");
        }
    }

    /**
     * Returns a cached schedule as a ByteSource.
     *
     * @return a cached schedule.
     */
    public ByteSource getSavedSchedule() {
        return Files.asByteSource(mDownloadedScheduleLocation);
    }

    /**
     * Starts an asynchronous update of a schedule.
     */
    public void startScheduleUpdate() {
        DownloadTask.startDownloading(WEB_SCHEDULE_URL, mDownloadedScheduleLocation);
    }
}
