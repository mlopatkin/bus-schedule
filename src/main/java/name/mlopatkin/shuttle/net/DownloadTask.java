/*
 * Copyright 2018 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.net;

import android.os.AsyncTask;
import android.util.Log;

import com.google.common.io.Files;

import name.mlopatkin.utils.android.Assertions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Async task that downloads a file from net. The resulting file is updated atomically.
 */
class DownloadTask extends AsyncTask<Void, Void, Boolean> {
    private static final String TAG = "DownloadTask";

    private final URL mSourceUrl;
    private final File mDestination;

    private DownloadTask(URL sourceUrl, File destination) {
        mSourceUrl = sourceUrl;
        mDestination = destination;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        try {
            Log.d(TAG, "Downloading the schedule");
            URLConnection connection = mSourceUrl.openConnection();
            File tempDestination = new File(mDestination + ".download");
            if (tempDestination.exists()) {
                if (!tempDestination.delete()) {
                    Log.e(TAG,
                            "Temporary destination file existed and it cannot be removed");
                    return false;
                }
            }
            try (InputStream input = connection.getInputStream()) {
                Files.asByteSink(tempDestination).writeFrom(input);
            }
            if (!tempDestination.renameTo(mDestination)) {
                Log.e(TAG, "Failed to copy downloaded file into final location");
                if (!tempDestination.delete()) {
                    Log.e(TAG, "Failed to clean up downloaded file");
                }
                return false;
            }
            return true;
        } catch (IOException e) {
            Log.e(TAG, "Failure during loading a schedule from net", e);
            return false;
        }
    }

    public static void startDownloading(String url, File destination) {
        try {
            new DownloadTask(new URL(url), destination).executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR);
        } catch (MalformedURLException e) {
            throw Assertions.notReached("Expecting URL '" + url + "' to be valid");
        }
    }
}
