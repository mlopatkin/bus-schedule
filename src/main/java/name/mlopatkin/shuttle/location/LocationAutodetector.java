/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.location;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;

import java.util.List;

import name.mlopatkin.shuttle.model.Location;

/**
 * This class converts positions (android.location.Location) into Locations from the model.
 * <p>
 * The rules are like this: if the nearest location is close enough to us then it is returned.
 * Otherwise a default location is returned.
 */
public class LocationAutodetector implements PositionProvider.PositionListener {

    public interface DetectedLocationListener {

        void onDetectedLocationUpdate(Location newLocation, boolean isNear);
    }

    private static final String EXACT_POS_PROVIDER = "exact";

    private static final float DETECTION_THRESHOLD_METERS = 1500.0f;

    private final PositionProvider mPositionProvider;

    private final List<Location> mLocations;
    private final Location mDefaultLocation;
    private Location mDetectedLocation;
    private boolean mDetectedLocationNear;

    private DetectedLocationListener mListener;

    private LocationAutodetector(PositionProvider positionProvider,
            List<Location> locations, Location defaultLocation) {
        mDefaultLocation = defaultLocation;
        mPositionProvider = positionProvider;
        mLocations = locations;
    }

    /**
     * Creates an Autodetector that uses default network-based position provider.
     *
     * @param positionProvider the position provider
     * @param locations        the list of locations
     * @param defaultLocation  the default location to return when others are too far away
     */
    public static LocationAutodetector create(
            PositionProvider positionProvider, List<Location> locations, Location defaultLocation) {
        Preconditions.checkNotNull(defaultLocation);
        Preconditions.checkArgument(locations.contains(defaultLocation));

        LocationAutodetector r =
                new LocationAutodetector(positionProvider, locations, defaultLocation);
        positionProvider.setPositionListener(r);
        return r;
    }

    public Location getDetectedLocation() {
        return MoreObjects.firstNonNull(mDetectedLocation, mDefaultLocation);
    }

    public boolean isDetectedLocationNear() {
        return mDetectedLocationNear;
    }

    public void setDetectedLocationListener(DetectedLocationListener listener) {
        mListener = listener;
    }

    public void start() {
        mPositionProvider.start();
    }

    public void stop() {
        mPositionProvider.stop();
    }

    private void setDetectedLocation(Location newLocation, boolean isNear) {
        mDetectedLocation = newLocation;
        mDetectedLocationNear = isNear;
        if (mListener != null) {
            mListener.onDetectedLocationUpdate(mDetectedLocation, isNear);
        }
    }

    private android.location.Location getPosition(Location location,
            android.location.Location pos) {
        if (pos == null) {
            pos = new android.location.Location(EXACT_POS_PROVIDER);
        }
        pos.setLatitude(location.getLat() / 1e6);
        pos.setLongitude(location.getLon() / 1e6);
        return pos;
    }

    @Override
    public void onPositionUpdated(android.location.Location position) {
        Location nearestLocation = mDefaultLocation;
        android.location.Location bestPosition = getPosition(nearestLocation, null);
        float bestDistance = position.distanceTo(bestPosition);
        android.location.Location otherPosition = new android.location.Location(EXACT_POS_PROVIDER);

        for (Location location : mLocations) {
            otherPosition = getPosition(location, otherPosition);
            float currentDistance = position.distanceTo(otherPosition);
            if (currentDistance < bestDistance) {
                bestPosition.set(otherPosition);
                bestDistance = currentDistance;
                nearestLocation = location;
            }
        }

        if (bestDistance < DETECTION_THRESHOLD_METERS) {
            setDetectedLocation(nearestLocation, true);
        } else {
            setDetectedLocation(mDefaultLocation, false);
        }
    }
}
