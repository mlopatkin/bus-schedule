/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.location;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import name.mlopatkin.shuttle.ShuttlesApp;
import name.mlopatkin.utils.android.Assertions;

/**
 * This class retrieves position from Android location providers. In Debug version it may use GPS
 * provider to allow location mocking, but generally it should use network.
 */
public class AndroidPositionProvider implements PositionProvider, LocationListener {

    private static final String TAG = "AndroidPositionProvider";

    private static final boolean USE_MOCK_LOCATIONS = false;
    private static final long LOCATION_UPDATE_INTERVAL_SHORT_MS = TimeUnit.SECONDS.toMillis(10);
    private static final long LOCATION_UPDATE_INTERVAL_MS = TimeUnit.MINUTES.toMillis(10);
    private static final float LOCATION_UPDATE_DISTANCE = 500.0f;

    private static AndroidPositionProvider sInstance;

    private final Context mContext;
    private final LocationManager mLocationManager;
    private final String mLocationProviderName;

    private PositionListener mListener;
    private Location mPositionEstimate;
    private boolean mRunning;

    AndroidPositionProvider(Context context, String locationProvider) {
        mLocationProviderName = locationProvider;
        mContext = context.getApplicationContext();
        mLocationManager = ShuttlesApp.getSystemService(mContext, Context.LOCATION_SERVICE);
        mPositionEstimate = mLocationManager.getLastKnownLocation(mLocationProviderName);
        Log.d(TAG, "Started with position estimate loc=" + mPositionEstimate);
    }

    public static PositionProvider get(Context context) {
        if (sInstance == null) {
            if (USE_MOCK_LOCATIONS) {
                Assertions.havePermission(context, Manifest.permission.ACCESS_FINE_LOCATION);
                sInstance = new AndroidPositionProvider(context, LocationManager.GPS_PROVIDER);
            } else {
                sInstance = new AndroidPositionProvider(context, LocationManager.NETWORK_PROVIDER);
            }
        }
        return sInstance;
    }

    @Nullable
    @Override
    public Location getPosition() {
        return mPositionEstimate;
    }

    @Override
    public void start() {
        if (mRunning) {
            return;
        }

        Log.d(TAG, "Starting requesting position updates");
        mRunning = true;
        // If there is no estimate yet then we should encourage updates, otherwise we don't have
        // location to show to a user
        long updateInterval = (mPositionEstimate == null)
                ? LOCATION_UPDATE_INTERVAL_SHORT_MS : LOCATION_UPDATE_INTERVAL_MS;
        mLocationManager.requestLocationUpdates(mLocationProviderName, updateInterval,
                LOCATION_UPDATE_DISTANCE, this);
    }

    @Override
    public void stop() {
        if (!mRunning) {
            return;
        }

        mRunning = false;
        Log.d(TAG, "Stopped requesting position updates");
        mLocationManager.removeUpdates(this);
    }

    @Override
    public void setPositionListener(@Nullable PositionListener listener) {
        if (mListener == listener) return;
        mListener = listener;
        if (mListener != null && mPositionEstimate != null) {
            mListener.onPositionUpdated(mPositionEstimate);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged loc=" + location);
        if (mPositionEstimate == null) {
            // reset to a longer update interval.
            stop();
            start();
        }
        mPositionEstimate = location;
        if (mListener != null) {
            mListener.onPositionUpdated(mPositionEstimate);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d(TAG, "onStatusChanged: provider=" + provider + " status=" + status);
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(TAG, "onProviderEnabled " + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(TAG, "onProviderDisabled " + provider);
    }
}
