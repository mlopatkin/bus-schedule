/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.location;

import android.location.Location;

import javax.annotation.Nullable;

/**
 * Provides information about current position of the user.
 */
public interface PositionProvider {

    public interface PositionListener {
        void onPositionUpdated(Location position);
    }

    /**
     * Returns last available position or {@code null} if there is no one.
     */
    @Nullable
    Location getPosition();

    /**
     * Starts periodic location updates.
     */
    void start();

    /**
     * Stops periodic location updates.
     */
    void stop();

    /**
     * Sets a listener to receive location updates. Set listener to null to clear it. If this
     * provider already has cached position then listener's method will be invoked immediately.
     */
    void setPositionListener(@Nullable PositionListener listener);
}
