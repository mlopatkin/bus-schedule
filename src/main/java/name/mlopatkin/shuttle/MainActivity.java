/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import dagger.Lazy;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasFragmentInjector;

import name.mlopatkin.shuttle.db.ShuttleDatabase;
import name.mlopatkin.shuttle.fullschedule.RouteListFragment;
import name.mlopatkin.shuttle.model.Location;
import name.mlopatkin.utils.android.GeoPoints;

import javax.inject.Inject;

/**
 * Main application activity.
 */
public class MainActivity extends BaseDbActivity implements HasFragmentInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> mDispatchingFragmentInjector;

    @Inject
    Lazy<LocationsSpinnerAdapter> mLocationsAdapter;

    @Inject
    ShuttleDatabase mDatabase;

    @Inject
    Preferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onDatabaseReady(Bundle savedInstanceState) {
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getActionBar();
        assert actionBar != null;
        actionBar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setListNavigationCallbacks(mLocationsAdapter.get(), (itemPosition, itemId) -> {
            mLocationsAdapter.get().setSelected(itemPosition);
            return true;
        });

        getActionBar().setSelectedNavigationItem(mLocationsAdapter.get().getSelectedPosition());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_action_bar, menu);
        menu.findItem(R.id.menu_show_on_map).setVisible(isShowOnMapAvaliable());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_show_on_map:
                openMapForCurrentLocation();
                break;
            case R.id.menu_about_app:
                AboutDialogs.showAboutDialog(this);
                break;
            case R.id.menu_show_timetable:
                RouteListFragment.showRouteSelector(this);
                break;
        }
        return true;
    }

    private void openMapForCurrentLocation() {
        if (mDatabase.isLoaded()) {
            Location location = mLocationsAdapter.get().getSelectedLocation();
            Uri locationGeoUri = GeoPoints.formatGeoUri(location.getLat(), location.getLon());
            startActivity(new Intent(Intent.ACTION_VIEW, locationGeoUri));
        }
    }

    private boolean isShowOnMapAvaliable() {
        PackageManager pm = getPackageManager();

        // totally fake, only scheme matter
        Intent openGeoIntent = new Intent(Intent.ACTION_VIEW, GeoPoints.formatGeoUri(0, 0));
        ResolveInfo info = pm.resolveActivity(openGeoIntent, 0);
        return info != null;
    }

    @Override
    public AndroidInjector<Fragment> fragmentInjector() {
        return mDispatchingFragmentInjector;
    }
}

