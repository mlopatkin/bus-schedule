/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.db;

import android.annotation.SuppressLint;
import android.content.Loader;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;
import com.google.common.io.ByteSource;

import name.mlopatkin.shuttle.model.Location;
import name.mlopatkin.shuttle.model.Route;
import name.mlopatkin.shuttle.model.Schedule;
import name.mlopatkin.shuttle.model.Tag;
import name.mlopatkin.shuttle.net.WebScheduleSource;
import name.mlopatkin.utils.ObserverList;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * DAO object for the database of shuttle buses. The DAO provides an async API
 * that returns {@link Loader}s.
 */
@Singleton
public class ShuttleDatabase {
    public interface Observer {
        void onScheduleLoaded();
    }

    private static final String TAG = "ShuttleDatabase";

    private final AssetScheduleSource mAssetScheduleSource;
    private final WebScheduleSource mWebScheduleSource;
    private final ObserverList<Observer> mObservers = new ObserverList<>();

    private Schedule mSchedule;
    private boolean mLoaded = false;

    /**
     * Creates a database object.
     */
    @Inject
    ShuttleDatabase(AssetScheduleSource assetScheduleSource,
            WebScheduleSource webScheduleSource) {
        mAssetScheduleSource = assetScheduleSource;
        mWebScheduleSource = webScheduleSource;
    }

    /**
     * Checks if the background loading process is finished.
     *
     * @return true if the database has been loaded
     */
    public boolean isLoaded() {
        return mLoaded;
    }

    /**
     * Kicks of loading of the DB data in background thread if DB isn't loaded yet.
     */
    public void startAsyncLoading() {
        if (isLoaded()) {
            return;
        }

        Log.d(TAG, "Loading database...");
        @SuppressLint("StaticFieldLeak")
        AsyncTask<Void, Void, Schedule> loadingTask = new AsyncTask<Void, Void, Schedule>() {
            @Override
            protected Schedule doInBackground(Void... params) {
                return loadAllDataSync();
            }

            @Override
            protected void onPostExecute(Schedule schedule) {
                mLoaded = true;
                mSchedule = schedule;
                for (Observer obs : mObservers) {
                    obs.onScheduleLoaded();
                }
                Log.d(TAG, "Database is ready");
            }
        };

        loadingTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @NonNull
    private Schedule loadAllDataSync() {
        Log.d(TAG, "Start loading");
        long startTime = SystemClock.uptimeMillis();
        try {
            Schedule schedule = fromFile();
            if (schedule == null) {
                schedule = fromAssets();
            }
            mWebScheduleSource.startScheduleUpdate();
            return schedule;
        } finally {
            Log.d(TAG, "Loading done " + (SystemClock.uptimeMillis() - startTime));
        }
    }

    public List<Location> getLocations() {
        Preconditions.checkState(mLoaded);
        return mSchedule.getLocations();
    }

    public Optional<Route> getRouteByTag(Tag<Route> tag) {
        return mSchedule.getRouteByTag(tag);
    }

    public void addObserver(Observer observer) {
        mObservers.addObserver(observer);
    }

    public void removeObserver(Observer observer) {
        mObservers.removeObserver(observer);
    }

    @Nullable
    private Schedule fromFile() {
        if (!mWebScheduleSource.isAvailable()) {
            return null;
        }
        try {
            return tryLoadSchedule(mWebScheduleSource.getSavedSchedule());
        } catch (JsonImporter.ImportException | IOException e) {
            Log.e(TAG, "Failed to load schedule from file", e);
            mWebScheduleSource.removeSavedSchedule();
        }
        return null;
    }

    @NonNull
    private Schedule fromAssets() {
        try {
            return tryLoadSchedule(mAssetScheduleSource.getScheduleFromAssets());
        } catch (JsonImporter.ImportException | IOException e) {
            throw new IllegalArgumentException("Can't parse a built-in schedule");
        }
    }

    private Schedule tryLoadSchedule(ByteSource source)
            throws IOException, JsonImporter.ImportException {
        return JsonImporter.importScheduleFromJson(source.asCharSource(Charsets.UTF_8));

    }
}
