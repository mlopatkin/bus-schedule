/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.db;

import android.content.Context;
import androidx.annotation.VisibleForTesting;

import com.google.common.io.ByteSource;

import name.mlopatkin.utils.android.AssetStreams;

import javax.inject.Inject;

public class AssetScheduleSource {
    private static final String SCHEDULE_ASSET_NAME = "schedule.json";
    private final Context mAppContext;
    private final String mScheduleAssetName;

    @Inject
    public AssetScheduleSource(Context appContext) {
        this(appContext, SCHEDULE_ASSET_NAME);
    }

    @VisibleForTesting
    AssetScheduleSource(Context context, String scheduleAssetName) {
        mAppContext = context;
        mScheduleAssetName = scheduleAssetName;
    }

    public ByteSource getScheduleFromAssets() {
        return AssetStreams.newInputStreamSupplier(mAppContext, mScheduleAssetName);
    }
}
