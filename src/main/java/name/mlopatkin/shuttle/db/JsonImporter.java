/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.db;

import com.google.common.base.Objects;
import com.google.common.io.CharSource;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import name.mlopatkin.shuttle.model.Location;
import name.mlopatkin.shuttle.model.Route;
import name.mlopatkin.shuttle.model.Schedule;
import name.mlopatkin.utils.android.JsonUtils;
import name.mlopatkin.utils.android.JsonUtils.UncheckedJSONException;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Imports db data from JSON file.
 */
public class JsonImporter {
    private final TimeZone mZone;
    private final Schedule.Builder mSchedule;
    private final Map<String, Location.Builder> mLocationByKey = new LinkedHashMap<>();

    private long mNextLocationId = 0;
    private long mNextRouteId = 0;
    private long mNextRouteEntryId = 0;

    private JsonImporter(CharSource charSource) throws ImportException,
            IOException {
        this(charSource.read());
    }

    /**
     * Parses supplied JSON string into information about bus schedule.
     *
     * @param data the data string
     * @throws ImportException if the schedule is inconsistent or JSON is
     *             malformed
     */
    private JsonImporter(String data) throws ImportException {
        mSchedule = new Schedule.Builder();
        try {
            JSONObject root = new JSONObject(data);
            mZone = loadTimeZone(root);
            loadLocations(root);
            loadRoutes(root);
        } catch (JSONException e) {
            throw new ImportException("Invalid JSON", e);
        }
    }

    private TimeZone loadTimeZone(JSONObject root) throws JSONException, ImportException {
        String timeZoneStr = root.getString("timezone");
        TimeZone zone = TimeZone.getTimeZone(timeZoneStr);
        if (Objects.equal(zone.getID(), timeZoneStr)) {
            return zone;
        } else {
            throw new ImportException("Invalid timezone: " + timeZoneStr);
        }
    }

    private void loadLocations(JSONObject root) throws JSONException {
        JSONArray locations = root.getJSONArray("locations");
        try {
            for (JSONObject locationObject : JsonUtils.asObjectIterable(locations)) {
                loadLocation(locationObject);
            }
        } catch (UncheckedJSONException e) {
            throw e.getCause();
        }
    }

    private void loadLocation(JSONObject locationObject) throws JSONException {
        String name = locationObject.getString("location_name");
        String key = locationObject.getString("key");
        String titleFrom = locationObject.getString("location_in_from");
        String titleTo = locationObject.getString("location_in_to");
        long lat = locationObject.getLong("lat");
        long lon = locationObject.getLong("lon");
        mLocationByKey.put(key,
                mSchedule.addLocation(mNextLocationId++, name, titleFrom, titleTo, mZone, lat,
                        lon));
    }

    private Location.Builder getLocationByKey(String key) throws ImportException {
        Location.Builder result = mLocationByKey.get(key);
        if (result == null) {
            throw new ImportException("Can't find location for key " + key);
        }
        return result;
    }

    private void loadRoutes(JSONObject root) throws JSONException, ImportException {
        JSONArray routes = root.getJSONArray("routes");
        for (JSONObject routeObj : JsonUtils.asObjectIterable(routes)) {
            loadRoute(routeObj);
        }
    }

    private void loadRoute(JSONObject route) throws JSONException, ImportException {
        String fromKey = route.getString("from");
        String toKey = route.getString("to");
        loadRouteEntries(
                getLocationByKey(fromKey).addRoute(mNextRouteId++, getLocationByKey(toKey)), route);
    }

    private void loadRouteEntries(Route.Builder route, JSONObject routeObject)
            throws JSONException {
        JSONArray entriesList = routeObject.getJSONArray("entries");
        for (JSONArray entry : JsonUtils.asArrayIterable(entriesList)) {
            int hour = entry.getInt(0);
            for (int minutes : JsonUtils.asIntIterable(entry.getJSONArray(1))) {
                route.addRouteEntry(mNextRouteEntryId++, hour, minutes);
            }
        }
    }

    /**
     * This exception is thrown when JSON data is malformed or there is an
     * inconsistency in provided data (unknown timezone, etc).
     */
    public static class ImportException extends Exception {

        ImportException(String detailMessage, Throwable throwable) {
            super(detailMessage, throwable);
        }

        ImportException(String detailMessage) {
            super(detailMessage);
        }
    }

    /**
     * Parses the content of the CharSource into information about bus schedule.
     *
     * @param source the SharSource that contains data in JSON format
     * @throws ImportException if the schedule is inconsistent or JSON is
     *             malformed
     * @throws IOException if reading fails
     */
    public static Schedule importScheduleFromJson(CharSource source)
            throws ImportException, IOException {
        return new JsonImporter(source).mSchedule.build();
    }
}
