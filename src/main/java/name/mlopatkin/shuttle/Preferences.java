/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import android.content.Context;
import android.content.SharedPreferences;

import name.mlopatkin.shuttle.model.Location;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Persistent application preferences.
 */
@Singleton
public class Preferences {

    private static final String PREF_FILE_NAME = "shuttle-prefs.xml";

    // Preference keys
    private static final String KEY_SELECTED_DASHBOARD_LOCATION = "selected dashboard location";

    private final SharedPreferences mPreferences;

    @Inject
    Preferences(Context context) {
        mPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Returns the id of selected location in dashboard or
     * {@code Location.NO_ID} if there wasn't selected any.
     *
     * @return the id of selected location
     */
    public long getSelectedDashboardLocationId() {
        return mPreferences.getLong(KEY_SELECTED_DASHBOARD_LOCATION, Location.NO_ID);
    }

    /**
     * Persists a selected location id.
     *
     * @param locationId the valid id of location or {@code Location.NO_ID}
     */
    public void setSelectedDashboardLocationId(long locationId) {
        mPreferences.edit().putLong(KEY_SELECTED_DASHBOARD_LOCATION, locationId).apply();
    }
}
