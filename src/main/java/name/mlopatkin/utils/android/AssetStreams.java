/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.utils.android;

import android.content.Context;
import android.content.res.AssetManager;

import com.google.common.io.ByteSource;

import java.io.IOException;
import java.io.InputStream;

/**
 * Guava-compatible wrappers for Android asset streams.
 */
public final class AssetStreams {
    private AssetStreams() {
        Assertions.staticOnly();
    }

    static class AssetByteSource extends ByteSource {

        private final String mAssetName;
        private final AssetManager mAssetManager;

        AssetByteSource(AssetManager assetManager, String assetName) {
            mAssetName = assetName;
            mAssetManager = assetManager;
        }

        @Override
        public InputStream openStream() throws IOException {
            return mAssetManager.open(mAssetName);
        }
    }

    /**
     * Creates a supplier for the asset. Returned supplier may hold a reference
     * to the context.
     *
     * @param context the context
     * @param assetName the name of asset
     * @return the supplier
     */
    public static ByteSource newInputStreamSupplier(Context context,
            String assetName) {
        return new AssetByteSource(context.getAssets(), assetName);
    }
}
