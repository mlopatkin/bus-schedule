/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.utils.android;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Property;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

import name.mlopatkin.shuttle.R;

/**
 * A listview that draws colored highlight over the item and animates it's change.
 * <p>
 * The implementation is limited (it may not handle deletion of the items well). Avoid changing
 * position of the highlighted item too often as well.
 */
public class HighlightListView extends ListView {

    public interface ListAdapterWithHighlight extends ListAdapter {
        int getHighlightPosition();
    }

    private static final long SCROLL_ANIMATION_DURATION_MS = 100;

    private ListAdapterWithHighlight mHighlightAdapter;

    private Paint mHighlightPaint;
    private Rect mHighlightRectCache = new Rect();
    private int mHighlightPosition = -1;
    private int mHighlightOffset;

    private Property<HighlightListView, Integer> mHighlighOffsetProperty =
            new Property<HighlightListView, Integer>(Integer.class, "highlightOffset") {
                @Override
                public Integer get(HighlightListView object) {
                    return object.mHighlightOffset;
                }

                @Override
                public void set(HighlightListView object, Integer value) {
                    object.mHighlightOffset = value;
                    invalidate();
                }
            };

    public HighlightListView(Context context) {
        super(context);
    }

    public HighlightListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public HighlightListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray style = context
                .getTheme()
                .obtainStyledAttributes(attrs, R.styleable.HighlightListView, defStyle, 0);
        try {
            if (style.hasValue(R.styleable.HighlightListView_highlightColor)) {
                setHighlightColor(style.getColor(R.styleable.HighlightListView_highlightColor, 0));
            }
        } finally {
            style.recycle();
        }
    }

    public void setHighlightColor(int color) {
        if (mHighlightPaint == null) {
            mHighlightPaint = new Paint();
        }
        mHighlightPaint.setColor(color);
    }

    public int getHighlightColor() {
        return mHighlightPaint != null ? mHighlightPaint.getColor() : 0;
    }

    @Override
    public void setAdapter(ListAdapter adapter) {
        setAdapter((ListAdapterWithHighlight) adapter);
    }

    public void setAdapter(ListAdapterWithHighlight adapter) {
        mHighlightAdapter = adapter;
        if (mHighlightAdapter == null) {
            mHighlightPosition = -1;
        }
        super.setAdapter(adapter);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        boolean needAnimation = false;
        int oldHighlightTop = 0;
        if (mHighlightAdapter != null) {
            int newHlPosition = mHighlightAdapter.getHighlightPosition();
            if (mHighlightPosition >= 0 && newHlPosition != mHighlightPosition) {
                // highlighted other position
                int oldHlIndex = mHighlightPosition - getFirstVisiblePosition();
                if (oldHlIndex >= 0 && oldHlIndex < getChildCount()) {
                    // old highlighted view is still here, animation is needed
                    View oldHighlightedView = getChildAt(oldHlIndex);
                    oldHighlightTop = oldHighlightedView.getTop();
                    needAnimation = true;
                }
            }
            mHighlightPosition = newHlPosition;
        }
        super.onLayout(changed, l, t, r, b);
        if (needAnimation) {
            int hlIndex = mHighlightPosition - getFirstVisiblePosition();
            if (hlIndex >= 0 && hlIndex < getChildCount()) {
                // scroll selection to position
                int newHighlightTop = getChildAt(hlIndex).getTop();
                mHighlightOffset = oldHighlightTop - newHighlightTop;
                ObjectAnimator.ofInt(this, mHighlighOffsetProperty, mHighlightOffset, 0)
                              .setDuration(SCROLL_ANIMATION_DURATION_MS)
                              .start();
            }
            // I'm too lazy to add proper handling of selection going out of sight, let it just
            // disappear
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mHighlightAdapter != null && mHighlightPaint != null) {
            int highlightPosition = mHighlightAdapter.getHighlightPosition();
            int firstVisiblePosition = getFirstVisiblePosition();
            if (highlightPosition >= firstVisiblePosition
                    && highlightPosition < firstVisiblePosition + getChildCount()) {
                drawHighlight(canvas, highlightPosition);
            }
        }
        super.onDraw(canvas);
    }

    private void drawHighlight(Canvas canvas, int highlightPosition) {
        View child = getChildAt(highlightPosition - getFirstVisiblePosition());
        Rect drawingRect = mHighlightRectCache;
        drawingRect.set(getPaddingLeft(), child.getTop() + mHighlightOffset,
                getWidth() - getPaddingRight(), child.getBottom() + mHighlightOffset);
        canvas.drawRect(drawingRect, mHighlightPaint);
    }
}
