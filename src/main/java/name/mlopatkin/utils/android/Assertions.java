/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.utils.android;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Utility methods to test correctness of run-time invariants.
 */
public final class Assertions {

    private Assertions() {
        staticOnly();
    }

    /**
     * Throws an {@link AssertionError} with the given message. Put this call in
     * the branch thaе should not be executed.
     *
     * @param message the message for a error
     * @throws AssertionError always
     * @return a error for you to "throw"
     */
    public static Error notReached(String message) {
        throw new AssertionError(message);
    }

    /**
     * To be used in constructors of static-only utility classes. Throws an
     * AssertionError immediately to prevent this class from instatinating.
     *
     * @throws AssertionError always
     * @see #notReached(String)
     */
    public static void staticOnly() {
        throw new AssertionError("static-only");
    }

    public static void havePermission(Context context, String permission) {
        if (context.getPackageManager().checkPermission(permission, context.getPackageName()) !=
                PackageManager.PERMISSION_GRANTED) {
            throw new AssertionError("Permission " + permission + "must be granted");
        }
    }
}
