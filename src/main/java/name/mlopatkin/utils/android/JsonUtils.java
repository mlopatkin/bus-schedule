/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.utils.android;

import com.google.common.collect.AbstractIterator;
import com.google.common.collect.FluentIterable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Helper methods to work with JSON data.
 */
public final class JsonUtils {

    private JsonUtils() {
        Assertions.staticOnly();
    }

    private interface JsonArrayExtractor<T> {
        T get(JSONArray array, int position) throws JSONException;
    }

    private static final JsonArrayExtractor<JSONObject> OBJECT_EXTRACTOR = JSONArray::getJSONObject;

    private static final JsonArrayExtractor<JSONArray> ARRAY_EXTRACTOR = JSONArray::getJSONArray;

    private static final JsonArrayExtractor<Integer> INT_EXTRACTOR = JSONArray::getInt;

    private static final class JsonArrayWrapper<T> extends FluentIterable<T> {

        private final JsonArrayExtractor<T> mExtractor;
        private JSONArray mArray;

        private JsonArrayWrapper(JsonArrayExtractor<T> extractor, JSONArray array) {
            mExtractor = extractor;
            mArray = array;
        }

        @Override
        public Iterator<T> iterator() {
            return new AbstractIterator<T>() {
                private int mPosition;

                @Override
                protected T computeNext() {
                    if (mPosition < mArray.length()) {
                        try {
                            return mExtractor.get(mArray, mPosition++);
                        } catch (JSONException e) {
                            throw new UncheckedJSONException(e);
                        }
                    } else {
                        return endOfData();
                    }
                }
            };
        }

        static <T> JsonArrayWrapper<T> from(JsonArrayExtractor<T> extractor, JSONArray array) {
            return new JsonArrayWrapper<>(extractor, array);
        }
    }

    /**
     * Wraps a {@link JSONArray} into {@link Iterable} to use in foreach loops.
     * It still can throw {@link UncheckedJSONException} during iteration if the
     * array access fails for some reason.
     *
     * @param array the array
     * @return the iterable that returns elements of the array as
     *         {@link JSONObject}s
     */
    public static FluentIterable<JSONObject> asObjectIterable(final JSONArray array) {
        return JsonArrayWrapper.from(OBJECT_EXTRACTOR, array);
    }

    /**
     * Wraps a {@link JSONArray} into {@link Iterable} to use in foreach loops.
     * It still can throw {@link UncheckedJSONException} during iteration if the
     * array access fails for some reason.
     *
     * @param array the array
     * @return the iterable that returns elements of the array as
     *         {@link JSONArray}s
     */
    public static FluentIterable<JSONArray> asArrayIterable(final JSONArray array) {
        return JsonArrayWrapper.from(ARRAY_EXTRACTOR, array);
    }

    /**
     * Wraps a {@link JSONArray} into {@link Iterable} to use in foreach loops.
     * It still can throw {@link UncheckedJSONException} during iteration if the
     * array access fails for some reason.
     *
     * @param array the array
     * @return the iterable that returns elements of the array as
     *         {@link Integer}s
     */
    public static FluentIterable<Integer> asIntIterable(final JSONArray array) {
        return JsonArrayWrapper.from(INT_EXTRACTOR, array);
    }

    /**
     * Wrapper for {@link JSONException} that can be thrown from places like
     * Runnable.run().
     */
    public static class UncheckedJSONException extends RuntimeException {
        /**
         * Wraps supplied argument.
         *
         * @param throwable the exception to wrap
         */
        public UncheckedJSONException(JSONException throwable) {
            super(throwable);
        }

        @Override
        public String getMessage() {
            return getCause().getMessage();
        }

        @Override
        public JSONException getCause() {
            return (JSONException) super.getCause();
        }
    }
}
