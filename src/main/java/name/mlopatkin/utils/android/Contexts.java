/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.utils.android;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

public final class Contexts {
    private Contexts() {
        Assertions.staticOnly();
    }

    public static void addScopedActivityCallbacks(Activity activity,
            Application.ActivityLifecycleCallbacks callbacks) {
        activity.getApplication()
                .registerActivityLifecycleCallbacks(new ScopedActivityWatcher(activity, callbacks));
    }


    private static class ScopedActivityWatcher implements Application.ActivityLifecycleCallbacks {

        private final Application.ActivityLifecycleCallbacks mCallbacks;
        private final Activity mActivityToWatch;

        ScopedActivityWatcher(Activity activityToWatch,
                Application.ActivityLifecycleCallbacks callbacks) {
            mCallbacks = callbacks;
            mActivityToWatch = activityToWatch;
        }

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
            if (mActivityToWatch == activity) {
                mCallbacks.onActivityCreated(activity, savedInstanceState);
            }
        }

        @Override
        public void onActivityStarted(Activity activity) {
            if (mActivityToWatch == activity) {
                mCallbacks.onActivityStarted(activity);
            }
        }

        @Override
        public void onActivityResumed(Activity activity) {
            if (mActivityToWatch == activity) {
                mCallbacks.onActivityResumed(activity);
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
            if (mActivityToWatch == activity) {
                mCallbacks.onActivityPaused(activity);
            }
        }

        @Override
        public void onActivityStopped(Activity activity) {
            if (mActivityToWatch == activity) {
                mCallbacks.onActivityStopped(activity);
            }
        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            if (mActivityToWatch == activity) {
                mCallbacks.onActivitySaveInstanceState(activity, outState);
            }
        }

        @Override
        public void onActivityDestroyed(Activity activity) {
            if (mActivityToWatch == activity) {
                mCallbacks.onActivityDestroyed(activity);
                // cleanup, because we hold a strong reference to the activity
                activity.getApplication().unregisterActivityLifecycleCallbacks(this);
            }
        }
    }
}
