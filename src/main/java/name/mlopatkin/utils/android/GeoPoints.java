/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.utils.android;

import android.location.Location;
import android.net.Uri;

import java.util.Locale;

public class GeoPoints {
    private GeoPoints() {
        Assertions.staticOnly();
    }

    /**
     * Formats geo point given as latitude and longitude multiplied by 10^6 as a geo uri.
     *
     * @param lat latitude * 10^6
     * @param lon longitude * 10^6
     * @return "geo:" Uri that points to the given point
     */
    public static Uri formatGeoUri(long lat, long lon) {
        String formattedPoint = formatPointAsUri(lat / 1e6, lon / 1e6);
        return Uri.parse("geo:" + formattedPoint);
    }

    private static String formatPointAsUri(double lat, double lon) {
        return String.format(Locale.US, "%.6f,%.6f", lat, lon);
    }

    public static float getDistance(Location from, long lat, long lon) {
        Location dest = new Location("exact_point");
        dest.setLatitude(lat / 1e6);
        dest.setLongitude(lon / 1e6);
        dest.setAccuracy(1.0f);
        return from.distanceTo(dest);
    }
}
