/*
 * Copyright 2014 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.utils.android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import static com.google.common.base.Preconditions.checkState;

import static name.mlopatkin.utils.android.Assertions.staticOnly;

/**
 * Utility methods to deal with views.
 */
public final class Views {

    private Views() {
        staticOnly();
    }

    /**
     * Attaches view holder with one line. Checks if there is a holder already.
     * <p>
     *
     * <pre>
     * return attachHolder(myView, new MyViewHolder(view));
     * </pre>
     *
     * @param view the view to attach holder to
     * @param viewHolder the holder to attach
     * @param <T> actual type of holder
     * @return the holder
     * @throws IllegalStateException if the view already has a tag attached
     */
    public static <T> T attachHolder(View view, T viewHolder) {
        checkState(view.getTag() == null, "Cannot attach holder, view tag isn't null");
        view.setTag(viewHolder);
        return viewHolder;
    }

    /**
     * Returns the result of View.findViewById automatically casted to appropriate type.
     */
    @SuppressWarnings("unchecked")
    public static <T extends View> T findViewById(View root, int id) {
        return (T) root.findViewById(id);
    }

    @SuppressWarnings("unchecked")
    public static <T extends View> T inflateDialogBody(Context context, int layoutId) {
        return (T) LayoutInflater.from(context).inflate(layoutId, null);
    }
}
