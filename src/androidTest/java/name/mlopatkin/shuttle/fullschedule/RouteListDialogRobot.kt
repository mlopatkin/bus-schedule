/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.fullschedule

import android.widget.ListView
import androidx.test.espresso.DataInteraction
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import name.mlopatkin.shuttle.model.Route
import name.mlopatkin.shuttle.test.listItems
import org.hamcrest.Matcher
import org.hamcrest.Matchers.anything
import org.hamcrest.Matchers.instanceOf

fun routeListDialog(block: RouteListDialogRobot.() -> Unit) {
    RouteListDialogRobot().apply(block)
}

class RouteListDialogRobot {
    fun hasItems(itemsMatcher: Matcher<in Iterable<Route>>) {
        onView(instanceOf(ListView::class.java))
                .check(ViewAssertions.matches(listItems(itemsMatcher)))
    }

    fun routeAtPosition(pos: Int): DataInteraction {
        return onData(anything()).atPosition(pos)
    }
}