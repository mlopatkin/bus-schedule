/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.fullschedule

import androidx.test.espresso.Espresso.pressBack
import androidx.test.ext.junit.runners.AndroidJUnit4
import name.mlopatkin.shuttle.BaseTest
import name.mlopatkin.shuttle.dashboardScreen
import name.mlopatkin.shuttle.test.ScreenOrientation
import name.mlopatkin.shuttle.test.TestData
import name.mlopatkin.shuttle.test.click
import org.hamcrest.Matchers
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RouteListDialogTest : BaseTest() {
    @Test
    fun routeListDialogHasAllRoutes() {
        launchMainActivity()

        dashboardScreen {
            openRoutesList()
        }

        routeListDialog {
            hasItems(Matchers.iterableWithSize(TestData.routes().size))
        }
    }

    @Test
    fun routesListDialogSurvivesRotation() {
        launchMainActivity()

        dashboardScreen {
            openRoutesList()
        }

        ScreenOrientation.LANDSCAPE.rotate(mMainActivityActivityTestRule.activity)

        routeListDialog {
            hasItems(Matchers.iterableWithSize(TestData.routes().size))
        }
    }

    @Test
    fun clickOnRouteOfRouteListDialogOpensFullschedule() {
        launchMainActivity()

        dashboardScreen {
            openRoutesList()
        }

        routeListDialog {
            routeAtPosition(0).click()
        }

        fullscheduleScreen {
            hasTitle(from = TestData.LOC_BENUA, to = TestData.LOC_NOVOCHERKASSKAYA)
            hasHighlightedDepartureAt("10:20")
        }
    }

    @Test
    fun routeListDialogIsClosedWithBackPress() {
        launchMainActivity()

        dashboardScreen {
            openRoutesList()
        }

        routeListDialog {
            pressBack()
        }

        dashboardScreen {
            isListDisplayed()
        }
    }
}