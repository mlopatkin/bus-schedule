/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.fullschedule

import android.view.View
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import name.mlopatkin.shuttle.model.Location
import name.mlopatkin.shuttle.model.RouteEntry
import name.mlopatkin.shuttle.test.ModelMatchers.getterMatcher
import name.mlopatkin.shuttle.test.TestData
import name.mlopatkin.shuttle.test.TimeUtils
import name.mlopatkin.shuttle.test.listView
import name.mlopatkin.utils.android.HighlightListView
import name.mlopatkin.utils.android.HighlightListView.ListAdapterWithHighlight
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.both
import org.hamcrest.Matchers.endsWith
import org.hamcrest.Matchers.startsWith
import org.hamcrest.TypeSafeMatcher

fun fullscheduleScreen(block: FullscheduleScreenRobot.() -> Unit) {
    FullscheduleScreenRobot().apply(block)
}

class FullscheduleScreenRobot {
    fun hasHighlightedDepartureAt(time: String) {
        onView(listView()).check(
                matches(adapterMatches(hasHighlightedItem(entryWithTime(time)))))
    }

    fun hasTitle(from: Location, to: Location) {
        onView(withText(both(startsWith(from.name)).and(endsWith(to.name)))).check(
                matches(isDisplayed()))
    }

    fun back() {
        Espresso.pressBack()
    }
}

private fun entryWithTime(departureTime: String): Matcher<RouteEntry> {
    val time = TimeUtils.offsetFromString(departureTime, TestData.TZ)
    return object : TypeSafeMatcher<RouteEntry>() {
        override fun matchesSafely(item: RouteEntry): Boolean {
            return time == item.time
        }

        override fun describeTo(description: Description) {
            description.appendValue(time)
        }
    }
}

private fun adapterMatches(
        adapterMatcher: Matcher<in ListAdapterWithHighlight>): Matcher<in View>? {
    return object : BoundedMatcher<View, HighlightListView>(HighlightListView::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("adapter =").appendDescriptionOf(adapterMatcher)
        }

        override fun matchesSafely(item: HighlightListView): Boolean {
            return adapterMatcher.matches(item.adapter)
        }
    }
}

@Suppress("UNCHECKED_CAST")
private fun <T> hasHighlightedItem(dataMatcher: Matcher<in T>): Matcher<ListAdapterWithHighlight> {
    return getterMatcher({ it?.getItem(it.highlightPosition) as T }, dataMatcher,
            "highlighted item")
}