/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle

import androidx.test.espresso.Espresso.pressBack
import androidx.test.ext.junit.runners.AndroidJUnit4
import name.mlopatkin.shuttle.test.ScreenOrientation
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class AboutDialogsTest : BaseTest() {

    @Before
    fun setupActivity() {
        launchMainActivity()
    }

    @Test
    fun aboutDialogIsClosedWithBackButton() {
        dashboardScreen {
            openAboutDialog()
        }

        aboutDialog {
            pressBack()
        }

        dashboardScreen {}
    }

    @Test
    fun aboutDialogIsClosedWithOkButton() {
        dashboardScreen {
            openAboutDialog()
        }

        aboutDialog {
            close()
        }

        dashboardScreen {}
    }

    @Test
    fun aboutDialogSurvivesRotation() {
        dashboardScreen {
            openAboutDialog()
        }

        ScreenOrientation.LANDSCAPE.rotate(mMainActivityActivityTestRule.activity)
        ScreenOrientation.PORTRAIT.rotate(mMainActivityActivityTestRule.activity)
        ScreenOrientation.DEFAULT.rotate(mMainActivityActivityTestRule.activity)

        aboutDialog {
            hasAboutText()
        }
    }
}