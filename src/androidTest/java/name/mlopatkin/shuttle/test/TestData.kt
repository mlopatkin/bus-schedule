/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package name.mlopatkin.shuttle.test

import name.mlopatkin.shuttle.model.Location
import name.mlopatkin.shuttle.model.Route
import name.mlopatkin.shuttle.model.RouteEntry
import name.mlopatkin.shuttle.model.Schedule
import name.mlopatkin.utils.android.Assertions
import org.json.JSONArray
import org.json.JSONObject
import java.util.TimeZone

data class TestPosition(val lat: Long, val lon: Long)


// Exact positions of bus stops (make sure to update fake_schedule.json if you are changing this)
val BENUA = TestPosition(59958745, 30406012)
val NOVOCHERKASSKAYA = TestPosition(59929446, 30412356)
val LENINA_SQUARE = TestPosition(59953963, 30356571)

// Neva Garden, across Piskarevskiy prospekt from the Benua
// https://yandex.ru/maps/-/CBR5ILvaxD
// ~250m from bus stop to Lenina square.
val NEAR_BENUA = TestPosition(59959736, 30408841)
// Near exit of "Lenina square" subway station, in the Finlyandskiy railway station.
// https://yandex.ru/maps/-/CBR5IXaecA
// ~170m from bus stop
val NEAR_LENINA = TestPosition(59955545, 30355532)

// Park Inn Hotel in Pulkovo airport
// https://yandex.ru/maps/-/CBR5I-tG~C
// ~18km from Lenina square
// ~16km from Novocherkasskaya
// ~19km from Benua
val FAR_AWAY = TestPosition(59799937, 30275896)

object TestData {
    val TZ = TimeZone.getTimeZone("Europe/Moscow")!!

    private val idToKey: MutableMap<Long, String> = HashMap()
    private var lastId: Long = 0

    private fun nextId(): Long {
        return ++lastId
    }

    private fun schedule(action: Schedule.Builder.() -> Unit): Schedule {
        val builder = Schedule.Builder()
        action(builder)
        return builder.build()
    }

    private fun Schedule.Builder.location(key: String, name: String, titleFrom: String,
                                          titleTo: String,
                                          position: TestPosition): Location.Builder {
        val id = nextId()
        idToKey[id] = key
        return addLocation(id, name, titleFrom, titleTo, TZ, position.lat,
                position.lon)
    }

    private fun route(from: Location.Builder, to: Location.Builder,
                      action: Route.Builder.() -> Unit) {
        val route = from.addRoute(nextId(), to)
        action(route)
    }

    private fun Route.Builder.entries(hours: Int, vararg minutes: Int) {
        for (minute in minutes) {
            addRouteEntry(nextId(), hours, minute)
        }
    }

    // This is a snapshot of the actual schedule used at the time of writing (Jan 2019).
    private val DEFAULT_SCHEDULE = schedule {
        val benua = location(name = "[ТЕСТ] БЦ Бенуа", titleFrom = "[ТЕСТ] От БЦ Бенуа",
                titleTo = "[ТЕСТ] К БЦ Бенуа", position = BENUA, key = "benua")
        val novocherkasskaya =
                location(name = "[ТЕСТ] Новочеркасская", titleFrom = "[ТЕСТ] От Новочеркасской",
                        titleTo = "[ТЕСТ] К Новочеркасской", position = NOVOCHERKASSKAYA,
                        key = "novocherkasskaya")
        val lenina = location(name = "[ТЕСТ] Площадь Ленина",
                titleFrom = "[ТЕСТ] От площади Ленина",
                titleTo = "[ТЕСТ] К площади Ленина", position = LENINA_SQUARE, key = "lenina")

        route(from = novocherkasskaya, to = benua) {
            entries(8, 45)
            entries(9, 10, 30, 50)
            entries(10, 10, 30, 50)
            entries(11, 10, 30, 50)
            entries(12, 10, 30, 50)
            entries(17, 40)
            entries(18, 0, 20, 40)
            entries(19, 0, 20, 40)
            entries(20, 0, 20, 40)
        }

        route(from = benua, to = novocherkasskaya) {
            entries(9, 15, 40)
            entries(10, 0, 20, 40)
            entries(11, 0, 20, 40)
            entries(12, 0, 20, 40)
            entries(13, 0)
            entries(17, 10, 30, 50)
            entries(18, 10, 30, 50)
            entries(19, 10, 30, 50)
            entries(20, 10, 30, 5)
        }


        route(from = lenina, to = benua) {
            entries(7, 20, 28, 36, 44, 52)
            entries(8, 0, 8, 16, 24, 32, 40, 48, 56)
            entries(9, 4, 12, 20, 28, 36, 44, 52)
            entries(10, 0, 8, 16, 24, 32, 40, 48, 56)
            entries(11, 4, 12, 20, 28, 36, 44, 52)
            entries(12, 0, 12, 24, 36, 48)
            entries(13, 0, 12, 24, 36, 48)
            entries(14, 0, 20, 40)
            entries(15, 0, 20, 40)
            entries(16, 0, 20, 40, 56)
            entries(17, 20, 28, 36, 44, 52)
            entries(18, 0, 8, 16, 24, 32, 40, 48, 56)
            entries(19, 4, 12, 20, 28, 36, 44, 52)
            entries(20, 0, 20, 40)
            entries(21, 0, 20, 40)
            entries(22, 0, 20, 40)
            entries(23, 15)
        }

        route(from = benua, to = lenina) {
            entries(7, 40, 48, 56)
            entries(8, 4, 12, 20, 28, 36, 44, 52)
            entries(9, 0, 8, 16, 24, 32, 40, 48, 56)
            entries(10, 4, 12, 20, 28, 36, 44, 52)
            entries(11, 0, 8, 16, 24, 32, 40, 48, 56)
            entries(12, 4, 12, 18, 30, 42, 54)
            entries(13, 6, 18, 30, 42, 54)
            entries(14, 20, 40)
            entries(15, 0, 20, 40)
            entries(16, 0, 20, 40)
            entries(17, 0, 8, 16, 24, 32, 40, 48, 56)
            entries(18, 4, 12, 20, 28, 36, 44, 52)
            entries(19, 0, 8, 16, 24, 32, 40, 48, 56)
            entries(20, 4, 12, 20, 40)
            entries(21, 0, 20, 40)
            entries(22, 0, 20, 40)
            entries(23, 0, 30)
        }
    }

    val LOC_BENUA = getLocationByKey("benua")
    val LOC_NOVOCHERKASSKAYA = getLocationByKey("novocherkasskaya")
    val LOC_LENINA = getLocationByKey("lenina")

    private fun Location.toJson(): JSONObject = json {
        put("location_name", name)
        put("location_in_from", titleFrom)
        put("location_in_to", titleTo)
        put("key", idToKey[id])
        put("lat", lat)
        put("lon", lon)
    }

    private fun Route.toJson(): JSONObject = json {
        put("from", idToKey[from.id])
        put("to", idToKey[to.id])
        put("entries", JSONArray(entries.map { it.toJson() }))
    }

    private fun RouteEntry.toJson(): JSONArray {
        return JSONArray(arrayOf(time.hours, JSONArray(arrayOf(time.minutes))))
    }

    fun toJsonString(): String {
        return json {
            put("timezone", "Europe/Moscow")
            put("locations", JSONArray(DEFAULT_SCHEDULE.locations.map { it.toJson() }))
            put("routes", JSONArray(
                    DEFAULT_SCHEDULE.locations.flatMap { it.routesFrom }.map { it.toJson() }))
        }.toString(2)
    }

    private fun getLocationByKey(key: String): Location {
        for (loc in DEFAULT_SCHEDULE.locations) {
            if (idToKey[loc.id] == key) {
                return loc
            }
        }

        throw Assertions.notReached("Invalid key")
    }

    private fun json(action: JSONObject.() -> Unit): JSONObject {
        val jsonObject = JSONObject()
        action(jsonObject)
        return jsonObject
    }

    fun routes(): List<Route> {
        return DEFAULT_SCHEDULE.locations.flatMap { it.routesFrom }
    }
}