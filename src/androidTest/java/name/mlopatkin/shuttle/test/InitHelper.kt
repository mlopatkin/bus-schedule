/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.test

import android.content.Intent
import android.os.Looper
import androidx.test.platform.app.InstrumentationRegistry
import name.mlopatkin.shuttle.HeartBeatSender
import name.mlopatkin.shuttle.TestShuttlesApp
import name.mlopatkin.shuttle.TestShuttlesApp.TestApplicationComponent
import java.util.concurrent.TimeUnit

fun initApplication(initClosure: TestApplicationComponent.() -> Unit) {
    runOnMainSync { TestShuttlesApp.getComponent().apply(initClosure) }
}

private fun runOnMainSync(closure: () -> Unit) {
    if (Looper.myLooper() == Looper.getMainLooper()) {
        closure()
    } else {
        InstrumentationRegistry.getInstrumentation().runOnMainSync(closure)
    }
}

fun advanceTime(timeDelta: Long, timeUnit: TimeUnit = TimeUnit.MINUTES) {
    initApplication {
        mockTimeSource.setCurrentTime(
                mockTimeSource.currentTimestamp + timeUnit.toMillis(timeDelta))
        InstrumentationRegistry.getInstrumentation().targetContext.sendBroadcast(
                Intent(HeartBeatSender.ACTION_HEARTBEAT_TEST))
    }
}