/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.test

import android.app.Activity
import android.content.pm.ActivityInfo
import android.view.View
import android.widget.ListView
import androidx.test.espresso.DataInteraction
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import org.hamcrest.Description
import org.hamcrest.Matcher

fun enumerateListItems(list: ListView): Iterable<*> {
    return object : Iterable<Any> {
        val adapter = list.adapter
        override fun iterator(): Iterator<Any> {
            return object : AbstractIterator<Any>() {
                private var mIndex = 0
                override fun computeNext() {
                    if (mIndex < adapter.count) {
                        setNext(adapter.getItem(mIndex++))
                    } else {
                        done()
                    }
                }
            }
        }
    }
}

fun <T> listItems(itemsMatcher: Matcher<in Iterable<T>>): Matcher<View> {
    return object : BoundedMatcher<View, ListView>(ListView::class.java) {
        override fun matchesSafely(listView: ListView): Boolean {
            return itemsMatcher.matches(enumerateListItems(listView))
        }

        override fun describeTo(description: Description) {
            itemsMatcher.describeTo(description)
        }
    }
}

/**
 * Matches a view with standard id `@android:id/list`.
 */
fun listView() : Matcher<View> {
    return ViewMatchers.withId(android.R.id.list)
}

fun DataInteraction.click() {
    this.perform(ViewActions.click())
}

enum class ScreenOrientation(private val orientation: Int) {
    DEFAULT(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED),
    PORTRAIT(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED),
    LANDSCAPE(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

    fun rotate(activity: Activity) {
        InstrumentationRegistry.getInstrumentation().runOnMainSync {
            activity.requestedOrientation = orientation
        }
        Espresso.onIdle()
    }
}
