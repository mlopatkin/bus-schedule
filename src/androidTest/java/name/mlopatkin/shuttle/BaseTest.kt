/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle

import android.content.Intent
import androidx.test.rule.ActivityTestRule
import name.mlopatkin.shuttle.test.NEAR_BENUA
import name.mlopatkin.shuttle.test.TimeUtils
import name.mlopatkin.shuttle.test.initApplication
import org.junit.Before
import org.junit.Rule

open class BaseTest {
    @Rule
    @JvmField
    val mMainActivityActivityTestRule = ActivityTestRule(
            MainActivity::class.java, true, false)

    @Before
    fun setUp() {
        initApplication {
            mockTimeSource.setCurrentTime(TimeUtils.parseTimestamp("2018-10-01 10:01+03:00"))
            fakePositionProvider.updatePosition(NEAR_BENUA, false)
            preferences.selectedDashboardLocationId = -1
        }
    }

    fun launchMainActivity() {
        mMainActivityActivityTestRule.launchActivity(Intent(Intent.ACTION_MAIN))
    }
}