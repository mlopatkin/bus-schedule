/*
 * Copyright 2018 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import android.os.Looper;
import androidx.annotation.MainThread;
import androidx.test.platform.app.InstrumentationRegistry;

import dagger.Component;
import dagger.android.AndroidInjectionModule;

import name.mlopatkin.shuttle.db.FakeDataModule;

import javax.inject.Singleton;

public class TestShuttlesApp extends ShuttlesApp {

    @Singleton
    @Component(modules = {ApplicationModule.class, AndroidInjectionModule.class,
            AppActivitiesModule.class, FakeDepsModule.class, FakeDataModule.class})
    public interface TestApplicationComponent extends ApplicationComponent {
        MockTimeSource getMockTimeSource();

        FakePositionProvider getFakePositionProvider();

        Preferences getPreferences();
    }

    private TestApplicationComponent mAppComponent;

    @Override
    protected TestApplicationComponent makeApplicationComponent() {
        return getAppComponent();
    }

    private TestApplicationComponent getAppComponent() {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            throw new AssertionError("UI Thread only!");
        }

        if (mAppComponent == null) {
            mAppComponent =
                    DaggerTestShuttlesApp_TestApplicationComponent.builder().applicationModule(
                            new ApplicationModule(this)).build();
        }
        return mAppComponent;
    }

    @MainThread
    public static TestApplicationComponent getComponent() {
        TestShuttlesApp app =
                (TestShuttlesApp) InstrumentationRegistry
                        .getInstrumentation().getTargetContext().getApplicationContext();
        return app.getAppComponent();
    }
}
