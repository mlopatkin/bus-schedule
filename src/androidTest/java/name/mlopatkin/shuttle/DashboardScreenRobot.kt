/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle

import android.view.View
import android.widget.Spinner
import androidx.test.espresso.DataInteraction
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withChild
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withTagValue
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import name.mlopatkin.shuttle.model.Location
import name.mlopatkin.shuttle.model.Route
import name.mlopatkin.shuttle.test.listItems
import name.mlopatkin.shuttle.test.listView
import org.hamcrest.Matcher
import org.hamcrest.Matchers.anything
import org.hamcrest.Matchers.both
import org.hamcrest.Matchers.equalTo
import org.hamcrest.Matchers.instanceOf
import org.hamcrest.Matchers.iterableWithSize

fun dashboardScreen(closure: DashboardScreenRobot.() -> Unit) {
    DashboardScreenRobot().apply {
        isListDisplayed()
        closure()
    }
}

class DashboardScreenRobot {
    private fun isRouteHeaderItem(): Matcher<Any> {
        return instanceOf(Route::class.java)
    }

    fun hasSelectedLocation(locationTitle: String) {
        onView(
                both(withChild(withText(locationTitle))).and(
                        instanceOf(Spinner::class.java)))
                .check(matches(isDisplayed()))
    }


    fun hasDashboard(verifier: DashboardItem.() -> Unit) {
        val dashboard = DashboardItem()
        dashboard.verifier()

        onView(listView()).check(matches(
                listItems<Any>(iterableWithSize(dashboard.verifiedCount()))))
    }

    class RouteItem(startPos: Int) {
        private var mEntryPos = startPos

        fun departure(matcher: Matcher<in View>) {
            matchListItem(matcher, mEntryPos++)
        }

        fun departureAt(time: String) {
            departure(withChild(both(withId(R.id.departure_time)).and(withTagValue(equalTo(time)))))
        }

        fun lastIndex(): Int {
            return mEntryPos
        }
    }

    class DashboardItem {
        private var currentPos = 0

        fun route(matcher: Matcher<in View>, matcherBuilder: RouteItem.() -> Unit) {
            matchListItem(matcher, currentPos++)
            currentPos = RouteItem(currentPos).apply(matcherBuilder).lastIndex()
        }

        fun routeWithTitle(title: String, matcherBuilder: RouteItem.() -> Unit) {
            route(withChild(
                    both(withId(R.id.location_title)).and(withText(title))),
                    matcherBuilder)
        }

        fun verifiedCount(): Int {
            return currentPos
        }
    }

    fun selectLocation(location: Location) {
        onView(instanceOf(Spinner::class.java)).perform(click())
        onData(equalTo(location)).perform(click())
    }

    fun routeAtPosition(i: Int): DataInteraction {
        return findListItem(isRouteHeaderItem()).atPosition(i)
    }

    fun openRoutesList() {
        onView(withId(R.id.menu_show_timetable)).perform(click())
    }

    fun isListDisplayed() {
        onView(listView()).check(matches(isDisplayed()))
    }

    fun openAboutDialog() {
        openActionBarOverflowOrOptionsMenu(
                InstrumentationRegistry.getInstrumentation().targetContext)

        onView(withText(R.string.menu_about_app)).perform(click())
    }
}

private fun matchListItem(matcher: Matcher<in View>, pos: Int) {
    onData(anything())
            .inAdapterView(listView())
            .atPosition(pos)
            .check(matches(matcher))
}

private fun findListItem(dataMatcher: Matcher<out Any>): DataInteraction {
    return onData(dataMatcher).inAdapterView(listView())
}
