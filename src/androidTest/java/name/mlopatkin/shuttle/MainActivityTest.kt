/*
 * Copyright 2018 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle

import androidx.test.ext.junit.runners.AndroidJUnit4
import name.mlopatkin.shuttle.fullschedule.fullscheduleScreen
import name.mlopatkin.shuttle.test.FAR_AWAY
import name.mlopatkin.shuttle.test.NEAR_BENUA
import name.mlopatkin.shuttle.test.TestData.LOC_BENUA
import name.mlopatkin.shuttle.test.TestData.LOC_LENINA
import name.mlopatkin.shuttle.test.TestData.LOC_NOVOCHERKASSKAYA
import name.mlopatkin.shuttle.test.TimeUtils
import name.mlopatkin.shuttle.test.advanceTime
import name.mlopatkin.shuttle.test.click
import name.mlopatkin.shuttle.test.initApplication
import org.hamcrest.Matchers.anything
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest : BaseTest() {

    @Test
    fun routesToDefaultLocationAreShownWhenStartedWithAutodetectedInFarAwayPosition() {
        initApplication {
            fakePositionProvider.updatePosition(FAR_AWAY, false)
        }

        launchMainActivity()

        dashboardScreen {
            hasSelectedLocation(LOC_BENUA.titleTo)

            hasDashboard {
                routeWithTitle(LOC_NOVOCHERKASSKAYA.titleFrom) {
                    departureAt("10:10")
                    departureAt("10:30")
                    departureAt("10:50")
                }

                routeWithTitle(LOC_LENINA.titleFrom) {
                    departureAt("10:08")
                    departureAt("10:16")
                    departureAt("10:24")
                }
            }
        }
    }

    @Test
    fun routesFromDefaultLocationAreShownWhenStartedWithAutodetectedInDefaultPosition() {
        launchMainActivity()

        dashboardScreen {
            hasSelectedLocation(LOC_BENUA.titleFrom)

            hasDashboard {
                routeWithTitle(LOC_NOVOCHERKASSKAYA.titleTo) {
                    departureAt("10:20")
                    departureAt("10:40")
                    departureAt("11:00")
                }

                routeWithTitle(LOC_LENINA.titleTo) {
                    departureAt("10:04")
                    departureAt("10:12")
                    departureAt("10:20")
                }
            }
        }
    }

    @Test
    @Ignore("Broken")
    fun routesFromSelectedLocationAreShownWhenPreferences() {
        initApplication {
            fakePositionProvider.updatePosition(NEAR_BENUA, false)
            preferences.selectedDashboardLocationId = LOC_NOVOCHERKASSKAYA.id
        }

        launchMainActivity()

        dashboardScreen {
            hasSelectedLocation(LOC_NOVOCHERKASSKAYA.titleFrom)

            hasDashboard {
                routeWithTitle(LOC_BENUA.titleTo) {
                    departure(anything())
                    departure(anything())
                    departure(anything())
                }
            }
        }
    }

    @Test
    fun shownEntriesChangeAsTimeAdvances() {
        launchMainActivity()

        advanceTime(10)

        dashboardScreen {
            hasSelectedLocation(LOC_BENUA.titleFrom)

            hasDashboard {
                routeWithTitle(LOC_NOVOCHERKASSKAYA.titleTo) {
                    departureAt("10:20")
                    departureAt("10:40")
                    departureAt("11:00")
                }

                routeWithTitle(LOC_LENINA.titleTo) {
                    departureAt("10:12")
                    departureAt("10:20")
                    departureAt("10:28")
                }
            }
        }
    }

    @Test
    fun onlyOneLocationIsDisplayedWhenOtherHasNoDeparturesLeft() {
        initApplication {
            mockTimeSource.setCurrentTime(TimeUtils.parseTimestamp("2018-10-01 20:41+03:00"))
        }

        launchMainActivity()

        dashboardScreen {
            hasSelectedLocation(LOC_BENUA.titleFrom)

            hasDashboard {
                routeWithTitle(LOC_LENINA.titleTo) {
                    departureAt("21:00")
                    departureAt("21:20")
                    departureAt("21:40")
                }
            }
        }
    }

    @Test
    fun selectingLocationInSpinnerChangesDashboard() {
        launchMainActivity()

        dashboardScreen {
            selectLocation(LOC_LENINA)

            hasSelectedLocation(LOC_LENINA.titleFrom)

            hasDashboard {
                routeWithTitle(LOC_BENUA.titleTo) {
                    departureAt("10:08")
                    departureAt("10:16")
                    departureAt("10:24")
                }
            }
        }
    }

    @Test
    fun clickOnRouteHeaderOpensFullschedule() {
        launchMainActivity()

        dashboardScreen {
            routeAtPosition(0).click()
        }

        fullscheduleScreen {
            hasTitle(from = LOC_BENUA, to = LOC_NOVOCHERKASSKAYA)
            hasHighlightedDepartureAt("10:20")
        }
    }
}
