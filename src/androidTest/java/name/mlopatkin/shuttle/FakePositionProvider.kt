/*
 * Copyright 2019 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle

import android.location.Location
import name.mlopatkin.shuttle.location.PositionProvider
import name.mlopatkin.shuttle.test.TestPosition
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FakePositionProvider @Inject constructor() : PositionProvider {
    private var mCurrentPos: Location? = null
    private var mListener: PositionProvider.PositionListener? = null
    private var mRunning = false

    override fun getPosition(): Location? {
        return mCurrentPos
    }

    override fun start() {
        mRunning = true
        notifyListener()
    }

    override fun stop() {
        mRunning = false
    }

    override fun setPositionListener(listener: PositionProvider.PositionListener?) {
        mListener = listener
    }

    fun updatePosition(position: TestPosition, notifyListener: Boolean = true) {
        updatePosition(position.lat, position.lon, notifyListener)
    }

    fun updatePosition(lat: Long, lon: Long, notifyListener: Boolean = true) {
        val newPos = Location("exact")
        newPos.latitude = lat.toDouble() / 1e6
        newPos.longitude = lon.toDouble() / 1e6
        mCurrentPos = newPos
        if (notifyListener) {
            notifyListener()
        }
    }

    fun notifyListener() {
        assert(mRunning)
        mListener?.onPositionUpdated(mCurrentPos)
    }
}