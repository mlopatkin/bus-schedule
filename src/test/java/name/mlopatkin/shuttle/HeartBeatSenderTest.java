/*
 * Copyright 2017 Mikhail Lopatkin                                         
 *                                                                         
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                         
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                         
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.
 */

package name.mlopatkin.shuttle;

import android.app.Application;
import android.content.Intent;

import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import name.mlopatkin.shuttle.test.TimeUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class HeartBeatSenderTest {

    @Mock
    private HeartBeatSender.Observer mObserver;
    @Mock
    private TimeSource mTimeSource;

    private HeartBeatSender mHeartBeatSender;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mHeartBeatSender = new HeartBeatSender(RuntimeEnvironment.application, mTimeSource);
    }

    @Test
    public void registeredObserverReceivesCallback() throws Exception {
        setCurrentTime("2017-10-08 22:25Z", TimeUtils.UTC);
        mHeartBeatSender.addObserver(mObserver);

        verify(mObserver).onTimeTick(false);
    }

    @Test
    public void senderNotifiesObserverOnMinuteChange() throws Exception {
        setCurrentTime("2017-10-08 22:25Z", TimeUtils.UTC);
        mHeartBeatSender.addObserver(mObserver);
        reset(mObserver);

        setCurrentTime("2017-10-08 22:26Z", TimeUtils.UTC);
        sendBroadcast(Intent.ACTION_TIME_TICK);

        verify(mObserver).onTimeTick(false);
    }

    @Test
    public void senderNotifiesObserverOnIntentToo() throws Exception {
        setCurrentTime("2017-10-08 22:25Z", TimeUtils.UTC);
        mHeartBeatSender.addObserver(mObserver);
        reset(mObserver);

        setCurrentTime("2017-10-08 22:25Z", TimeUtils.UTC);
        sendBroadcast(Intent.ACTION_TIME_TICK);

        verify(mObserver).onTimeTick(false);
    }

    @Test
    public void observerStopsReceivingEventsAfterUnregister() throws Exception {
        setCurrentTime("2017-10-08 22:25Z", TimeUtils.UTC);
        HeartBeatSender.Observer otherObserver = mock(HeartBeatSender.Observer.class);
        mHeartBeatSender.addObserver(mObserver);
        mHeartBeatSender.addObserver(otherObserver);
        reset(mObserver);

        mHeartBeatSender.removeObserver(mObserver);
        setCurrentTime("2017-10-08 22:26Z", TimeUtils.UTC);
        sendBroadcast(Intent.ACTION_TIME_TICK);

        verify(mObserver, never()).onTimeTick(anyBoolean());
    }

    @Test
    public void dayChangeCausesInvalidation() throws Exception {
        setCurrentTime("2017-10-08 23:59Z", TimeUtils.UTC);
        mHeartBeatSender.addObserver(mObserver);
        reset(mObserver);

        setCurrentTime("2017-10-09 00:00Z", TimeUtils.UTC);
        sendBroadcast(Intent.ACTION_TIME_TICK);

        verify(mObserver).onTimeTick(true);
    }

    @Test
    public void backwardTimeAdjustementCausesInvalidation() throws Exception {
        setCurrentTime("2017-10-08 22:25Z", TimeUtils.UTC);
        mHeartBeatSender.addObserver(mObserver);
        reset(mObserver);

        setCurrentTime("2017-10-08 22:24Z", TimeUtils.UTC);
        sendBroadcast(Intent.ACTION_TIME_CHANGED);

        verify(mObserver).onTimeTick(true);
    }

    @Test
    public void timezoneChangeCausesInvalidation() throws Exception {
        setCurrentTime("2017-10-08 22:25Z", TimeUtils.UTC);
        mHeartBeatSender.addObserver(mObserver);
        reset(mObserver);

        setCurrentTime("2017-10-08 22:25Z", TimeUtils.MSK);
        sendBroadcast(Intent.ACTION_TIMEZONE_CHANGED);

        verify(mObserver).onTimeTick(true);
    }


    @Test
    public void newlyConstructedSenderRegistersNoReceivers() throws Exception {
        Application application = RuntimeEnvironment.application;
        ShadowApplication shadowApplication = Shadows.shadowOf(application);
        shadowApplication.assertNoBroadcastListenersOfActionRegistered(application,
                Intent.ACTION_TIME_CHANGED);
        shadowApplication.assertNoBroadcastListenersOfActionRegistered(application,
                Intent.ACTION_TIME_TICK);
        shadowApplication.assertNoBroadcastListenersOfActionRegistered(application,
                Intent.ACTION_TIMEZONE_CHANGED);
    }

    @Test
    public void pausedSenderUnregisterReceivers() throws Exception {
        mHeartBeatSender.addObserver(mObserver);
        mHeartBeatSender.removeObserver(mObserver);

        Application application = RuntimeEnvironment.application;
        ShadowApplication shadowApplication = Shadows.shadowOf(application);
        shadowApplication.assertNoBroadcastListenersOfActionRegistered(application,
                Intent.ACTION_TIME_CHANGED);
        shadowApplication.assertNoBroadcastListenersOfActionRegistered(application,
                Intent.ACTION_TIME_TICK);
        shadowApplication.assertNoBroadcastListenersOfActionRegistered(application,
                Intent.ACTION_TIMEZONE_CHANGED);
    }

    private void setCurrentTime(String currentTime, TimeZone tz) {
        when(mTimeSource.getCurrentTime()).thenAnswer(invocation -> {
            Calendar result = Calendar.getInstance();
            Date dateTime = TimeUtils.parseDateTime(currentTime);
            result.setTimeZone(tz);
            result.setTime(dateTime);
            return result;
        });
    }

    private void sendBroadcast(String action) {
        RuntimeEnvironment.application.sendBroadcast(new Intent(action));
    }
}