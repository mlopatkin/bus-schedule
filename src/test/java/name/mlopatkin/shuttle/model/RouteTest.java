/*
 * Copyright 2017 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.model;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matcher;
import org.junit.Test;

import name.mlopatkin.shuttle.test.ModelMatchers;
import name.mlopatkin.shuttle.test.TimeUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class RouteTest {
    private final SimpleDateFormat mTimeParser = new SimpleDateFormat("HH:mm");

    @SuppressWarnings("unchecked")
    @Test
    public void getNextEntries() throws Exception {
        Route route = buildRouteWithEntries("10:00", "10:10", "10:15");

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 09:00Z")),
                contains(entryWithTime("10:00"), entryWithTime("10:10"), entryWithTime("10:15")));

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 10:00Z")),
                contains(entryWithTime("10:00"), entryWithTime("10:10"), entryWithTime("10:15")));

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 10:01Z")),
                contains(entryWithTime("10:10"), entryWithTime("10:15")));

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 10:14Z")),
                contains(entryWithTime("10:15")));

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 10:15Z")),
                contains(entryWithTime("10:15")));

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 10:16Z")), empty());
    }

    @Test
    public void getNextEntries_routeWithSingleEntry() throws Exception {
        Route route = buildRouteWithEntries("10:00");

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 09:00Z")),
                contains(entryWithTime("10:00")));

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 10:00Z")),
                contains(entryWithTime("10:00")));

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 10:01Z")), empty());
    }

    @Test
    public void getNextEntries_newDayStartsAtMidnight() throws Exception {
        Route route = buildRouteWithEntries("10:00");

        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-08 23:59Z")), empty());
        assertThat(route.getNextEntries(TimeUtils.parseTimestamp("2017-10-09 00:00Z")),
                contains(entryWithTime("10:00")));
    }

    @Test
    public void getTag_ReturnsTagForTheRoute() throws Exception {
        Route route = buildRouteWithEntries("10:00");
        assertThat(route.getTag().isTagFor(route), is(true));
    }

    private Route buildRouteWithEntries(String... times) throws Exception {
        Schedule.Builder scheduleBuilder = new Schedule.Builder();
        Location.Builder loc1Builder = scheduleBuilder.addLocation(0, "loc1", "loc1", "loc1",
                TimeUtils.UTC, 0, 0);
        Location.Builder loc2Builder = scheduleBuilder.addLocation(0, "loc2", "loc2", "loc2",
                TimeUtils.UTC, 0, 0);
        Route.Builder builder = loc1Builder.addRoute(0, loc2Builder);
        for (String time : times) {
            appendEntryFromString(builder, time);
        }
        return scheduleBuilder.build().getLocations().get(0).getRoutesFrom().get(0);
    }

    private void appendEntryFromString(Route.Builder builder, String time) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mTimeParser.parse(time));
        builder.addRouteEntry(0, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
    }

    private static Matcher<RouteEntry> entryWithTime(String departureTime) {
        OffsetTime time = TimeUtils.offsetFromString(departureTime);
        return ModelMatchers.getterMatcher(RouteEntry::getTime, equalTo(time), "Departure at");
    }
}