/*
 * Copyright 2017 Mikhail Lopatkin
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package name.mlopatkin.shuttle.model;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.everyItem;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import static name.mlopatkin.shuttle.test.ModelMatchers.hasDestination;
import static name.mlopatkin.shuttle.test.ModelMatchers.hasOrigin;
import static name.mlopatkin.shuttle.test.ModelMatchers.isBetween;

import java.util.TimeZone;

public class ScheduleTest {

    @SuppressWarnings("unchecked")
    @Test
    public void routePointsAreSetProperly() throws Exception {
        Schedule.Builder schBuilder = new Schedule.Builder();
        Location.Builder loc1 = schBuilder.addLocation(1, "loc1", "From Loc 1", "To Loc 1",
                TimeZone.getDefault(), 0, 0);
        Location.Builder loc2 = schBuilder.addLocation(2, "loc2", "From Loc 2", "To Loc 2",
                TimeZone.getDefault(), 1, 1);
        Location.Builder loc3 = schBuilder.addLocation(2, "loc3", "From Loc 3", "To Loc 3",
                TimeZone.getDefault(), 2, 3);

        loc1.addRoute(1, loc2).addRouteEntry(1, 0, 0);
        loc1.addRoute(2, loc3).addRouteEntry(2, 0, 0);
        loc2.addRoute(3, loc1).addRouteEntry(3, 0, 0);
        loc3.addRoute(4, loc1).addRouteEntry(4, 0, 0);

        Schedule schedule = schBuilder.build();

        Location l1 = schedule.getLocations().get(0);
        Location l2 = schedule.getLocations().get(1);
        Location l3 = schedule.getLocations().get(2);


        assertThat(l1.getRoutesFrom(), everyItem(hasOrigin(l1)));
        assertThat(l1.getRoutesFrom(),
                contains(hasDestination(l2), hasDestination(l3)));

        assertThat(l1.getRoutesTo(), everyItem(hasDestination(l1)));
        assertThat(l1.getRoutesTo(),
                containsInAnyOrder(hasOrigin(l2), hasOrigin(l3)));

        assertThat(l2.getRoutesFrom(), everyItem(hasOrigin(l2)));
        assertThat(l2.getRoutesFrom(), contains(hasDestination(l1)));

        assertThat(l2.getRoutesTo(), contains(isBetween(l1, l2)));

        assertThat(l3.getRoutesFrom(), everyItem(hasOrigin(l3)));
        assertThat(l3.getRoutesFrom(), contains(hasDestination(l1)));

        assertThat(l3.getRoutesTo(), contains(isBetween(l1, l3)));
    }

}