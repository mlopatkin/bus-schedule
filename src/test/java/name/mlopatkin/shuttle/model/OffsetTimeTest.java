/*
 * Copyright 2017 Mikhail Lopatkin                                         
 *                                                                         
 * Licensed under the Apache License, Version 2.0 (the "License");         
 * you may not use this file except in compliance with the License.        
 * You may obtain a copy of the License at                                 
 *                                                                         
 *     http://www.apache.org/licenses/LICENSE-2.0                          
 *                                                                         
 * Unless required by applicable law or agreed to in writing, software     
 * distributed under the License is distributed on an "AS IS" BASIS,       
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and     
 * limitations under the License.
 */

package name.mlopatkin.shuttle.model;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import name.mlopatkin.shuttle.test.TimeUtils;

public class OffsetTimeTest {

    @Test
    public void equalValuesAreEqual() throws Exception {
        assertThat(new OffsetTime(13, 37, TimeUtils.PDT),
                equalTo(new OffsetTime(13, 37, TimeUtils.PDT)));
    }

    @Test
    public void differentHoursArentEqual() throws Exception {
        assertThat(new OffsetTime(14, 37, TimeUtils.PDT),
                not(equalTo(new OffsetTime(13, 37, TimeUtils.PDT))));
    }

    @Test
    public void differentMinutesArentEqual() throws Exception {
        assertThat(new OffsetTime(13, 38, TimeUtils.PDT),
                not(equalTo(new OffsetTime(13, 37, TimeUtils.PDT))));
    }

    @Test
    public void differentTimezonesArentEqual() throws Exception {
        assertThat(new OffsetTime(13, 37, TimeUtils.PDT),
                not(equalTo(new OffsetTime(13, 37, TimeUtils.MSK))));
    }

    @Test
    public void toStringFormatsTimeProperly() throws Exception {
        assertThat(new OffsetTime(13, 37, TimeUtils.PDT).toString(),
                startsWith("OffsetTime{13:37-07:00 /"));
        assertThat(new OffsetTime(1, 2, TimeUtils.MSK).toString(),
                startsWith("OffsetTime{01:02+03:00 /"));
        assertThat(new OffsetTime(0, 0, TimeUtils.UTC).toString(),
                startsWith("OffsetTime{00:00+00:00 /"));

        assertThat(new OffsetTime(2, 30, TimeUtils.NST).toString(),
                startsWith("OffsetTime{02:30+05:45 /"));
    }

    @Test
    public void getTodayInstant_isNow() throws Exception {
        long now = TimeUtils.parseTimestamp("2017-10-04 01:53+03:00");

        assertThat(new OffsetTime(1, 53, TimeUtils.MSK).getTodayInstant(now), equalTo(now));
    }

    @Test
    public void getTodayInstant_isEarlier() throws Exception {
        long expectedInstant = TimeUtils.parseTimestamp("2017-10-04 00:50+03:00");
        long now = TimeUtils.parseTimestamp("2017-10-04 01:53+03:00");

        assertThat(new OffsetTime(0, 50, TimeUtils.MSK).getTodayInstant(now),
                equalTo(expectedInstant));
    }

    @Test
    public void getTodayInstant_isLater() throws Exception {
        long expectedInstant = TimeUtils.parseTimestamp("2017-10-04 12:50-07:00");
        long now = TimeUtils.parseTimestamp("2017-10-04 01:53-07:00");

        assertThat(new OffsetTime(12, 50, TimeUtils.PDT).getTodayInstant(now),
                equalTo(expectedInstant));
    }

    @Test
    public void getTodayInstant_isEarlierForMs() throws Exception {
        long expectedInstant = TimeUtils.parseTimestamp("2017-10-04 12:50-07:00");
        long now = expectedInstant + 10;

        assertThat(new OffsetTime(12, 50, TimeUtils.PDT).getTodayInstant(now),
                equalTo(expectedInstant));
    }

    @Test
    public void getTodayInstant_isLaterForMs() throws Exception {
        long expectedInstant = TimeUtils.parseTimestamp("2017-10-04 12:50-07:00");
        long now = expectedInstant - 10;

        assertThat(new OffsetTime(12, 50, TimeUtils.PDT).getTodayInstant(now),
                equalTo(expectedInstant));
    }

}