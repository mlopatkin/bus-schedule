package name.mlopatkin.shuttle.location;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import name.mlopatkin.shuttle.model.Location;

import java.util.Arrays;
import java.util.List;

@RunWith(RobolectricTestRunner.class)
@Config(manifest = Config.NONE)
public class LocationAutodetectorTest {

    List<Location> mLocations = Arrays.asList(
            new Location("B", "B", "B", 59958745, 30406012),
            new Location("N", "N", "N", 59931698, 30410124),
            new Location("L", "N", "N", 59953963, 30356571)
    );

    @Mock
    private PositionProvider mProvider;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testOnLocationUpdate_nearDefault() throws Exception {
        LocationAutodetector detector =
                LocationAutodetector.create(mProvider, mLocations, mLocations.get(0));

        android.location.Location location = new android.location.Location("exact");

        location.setLatitude(59.958741);
        location.setLongitude(30.406011);
        detector.onPositionUpdated(location);

        assertEquals(mLocations.get(0), detector.getDetectedLocation());
        assertTrue(detector.isDetectedLocationNear());
    }

    @Test
    public void testOnLocationUpdated_nearNonDefault() throws Exception {
        LocationAutodetector detector =
                LocationAutodetector.create(mProvider, mLocations, mLocations.get(0));

        android.location.Location location = new android.location.Location("exact");

        location.setLatitude(59.931271);
        location.setLongitude(30.407693);
        detector.onPositionUpdated(location);

        assertEquals(mLocations.get(1), detector.getDetectedLocation());
        assertTrue(detector.isDetectedLocationNear());
    }

    @Test
    public void testOnLocationUpdate_farAway() throws Exception {
        LocationAutodetector detector =
                LocationAutodetector.create(mProvider, mLocations, mLocations.get(0));

        android.location.Location location = new android.location.Location("exact");

        location.setLatitude(59.905421);
        location.setLongitude(30.326483);
        detector.onPositionUpdated(location);

        assertEquals(mLocations.get(0), detector.getDetectedLocation());
        assertFalse(detector.isDetectedLocationNear());
    }

    @Test
    public void testGetDefaultLocation_LocationProviderUninitialized() throws Exception {
        Mockito.when(mProvider.getPosition()).thenReturn(null);

        LocationAutodetector detector =
                LocationAutodetector.create(mProvider, mLocations, mLocations.get(0));
        assertEquals(mLocations.get(0), detector.getDetectedLocation());
        assertFalse(detector.isDetectedLocationNear());
    }
}
